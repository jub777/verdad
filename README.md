# Verdad: A Magmide Implementation

## Running Playground

- Install trunk with `cargo install --locked trunk`
- Add wasm32 as a target for rust `rustup target add wasm32-unknown-unknown`
- Serve website with `trunk serve ./playground/index.html`
- Use playground at `http://127.0.0.1:8080/index.htm#dev`
    - NOTE in firefox you get weird caching issues, use Chrome if developing

## Important Links

- The magmide project: https://github.com/magmide/magmide
- COQ learning guide: https://softwarefoundations.cis.upenn.edu/

