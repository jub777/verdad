#!/bin/bash

set -eu

tag="drgabble/images:rust"
docker build . -f docker/Dockerfile -t "$tag"
docker push "$tag"
