# Contributing

Contributions very welcome! Please get in touch with Jacob to start contributing.

## Jacob Is Dictator

For now, to keep vision and direction, Jacob will review all code before being committed to the repo. This will primarily be done via pair-programming, or by weekly reviews. This (and the Branching Strategy) will need to be revisited as the project grows beyond a few people.

## Wide, Not Deep

Each feature should be added so that it works with all the tooling: playground, curator, auto formating, linting etc. That goal is that the project should always be as polished as it could be, without adding additional features. We should add error cases and nice tooling sooner rather than later, to avoid tech debt.

## Code Architecture

In future, the current top-level modules in `verdad` workspace will probably become their own repositories. So care should be taken to minimise cross-dependancies between these modules to make compile times faster.

## Test Driven Development

Contributions should be written using [Test Driven Development (TDD)](https://en.wikipedia.org/wiki/Test-driven_development). This ensures that we have good test coverage to aid documentation and refactoring.

## Branching Strategy

For now, while the project is small, all commits should be made directly to master, to avoid branches from diverging. Commits should be made frequently, as soon as code compiles and tests pass, to reduce merge conflicts. Care should be taken when adding large changes or refactoring to ensure that changes don't break anything.

## Readability First

The primary goal of all the code should be to make it readable and easily understandable above all else. Optimization can be done later.
