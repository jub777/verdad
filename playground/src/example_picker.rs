const EXAMPLE_DIRECTORY: include_dir::Dir<'_> =
    include_dir::include_dir!("$CARGO_MANIFEST_DIR/../examples/");

pub fn example_picker(ui: &mut egui::Ui) -> Option<&'static str> {
    let mut source = None;
    egui::ScrollArea::vertical().show(ui, |ui| {
        ui.with_layout(egui::Layout::top_down(egui::Align::LEFT), |ui| {
            ui.label("Examples: ");
            add_child_buttons(&mut source, ui, &EXAMPLE_DIRECTORY);
        });
    });
    source
}

fn add_child_buttons(
    source: &mut Option<&'static str>,
    ui: &mut egui::Ui,
    parent: &'static include_dir::Dir<'static>,
) {
    for file in parent.files() {
        add_file_button(source, ui, file);
    }
    for directory in parent.dirs() {
        add_child_buttons(source, ui, directory);
    }
}

fn add_file_button(
    source: &mut Option<&'static str>,
    ui: &mut egui::Ui,
    file: &'static include_dir::File<'static>,
) {
    let name = file.path().to_str().unwrap();
    if ui.add(egui::Button::new(name)).clicked() {
        *source = Some(file.contents_utf8().unwrap());
    }
}
