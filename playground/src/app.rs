use serde::{Deserialize, Serialize};

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(Default, Deserialize, Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    source: String,
}

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        cc.egui_ctx.set_visuals(egui::Visuals::dark());

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }

        Default::default()
    }

    fn format_source(&mut self, context: &egui::Context, id: egui::Id) -> Option<()> {
        let mut state = egui::TextEdit::load_state(context, id)?;
        let old_cursor = state.ccursor_range()?;
        let (formatted, new_cursor) = crate::source::format_source(&self.source, old_cursor)?;
        state.set_ccursor_range(Some(new_cursor));
        egui::TextEdit::store_state(context, id, state);
        self.source = formatted;
        Some(())
    }
}

impl eframe::App for TemplateApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    fn update(&mut self, context: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("examples_panel").show(context, |ui| {
            if let Some(source) = crate::example_picker::example_picker(ui) {
                self.source = source.to_string();
            }
        });

        egui::SidePanel::right("error_panel").show(context, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.add_sized(
                    ui.available_size(),
                    egui::TextEdit::multiline(&mut self.source.as_str())
                        .layouter(&mut crate::error::error_messages_layouter),
                );
            });
        });

        egui::CentralPanel::default().show(context, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                    let response = ui.add_sized(
                        ui.available_size(),
                        egui::TextEdit::multiline(&mut self.source)
                            .layouter(&mut crate::source::source_layouter)
                            .code_editor(),
                    );
                    if response.changed() {
                        self.format_source(context, response.id);
                    }
                });
            });
        });
    }
}
