#![warn(clippy::all, rust_2018_idioms)]

mod app;
mod error;
mod example_picker;
mod layout_job_writer;
mod source;

pub use app::TemplateApp;
