use std::sync::Arc;

use codespan_reporting::{files::SimpleFiles, term::termcolor::WriteColor};
use egui::{text::LayoutJob, Galley};
use verdad::{BoxedError, Error};

use crate::layout_job_writer::LayoutJobWriter;

pub fn error_messages_layouter(ui: &egui::Ui, source: &str, _: f32) -> Arc<Galley> {
    let mut job = LayoutJob::default();
    let font = egui::FontId::monospace(20.0);
    let mut output = LayoutJobWriter::new(&mut job, font);
    write_messages(&mut output, source);
    ui.fonts(|f| f.layout_job(job))
}

fn write_messages(output: &mut impl WriteColor, source: &str) {
    match verdad::check(source) {
        Ok(warnings) if !warnings.is_empty() => {
            write!(output, "{} warnings:\n\n", warnings.len()).unwrap();
            write_warnings(source, warnings, output);
        }
        Err(error) => {
            write_error(source, error, output);
        }
        _ => write!(output, "No issues! Well done.").unwrap(),
    }
}

fn write_error(source: &str, error: BoxedError, output: &mut impl WriteColor) {
    write_error_diagnostic(source, error.as_ref(), output);
}

fn write_warnings(source: &str, warnings: Vec<BoxedError>, output: &mut impl WriteColor) {
    for warning in warnings.iter() {
        write_error_diagnostic(source, warning.as_ref(), output);
    }
}

fn write_error_diagnostic(source: &str, error: &dyn Error, output: &mut impl WriteColor) {
    let mut files: SimpleFiles<&str, &str> = SimpleFiles::new();
    let file_id = files.add("(playground)", source);
    let config = codespan_reporting::term::Config::default();
    let diagnostic = error.diagnostic(file_id);
    codespan_reporting::term::emit(output, &config, &files, &diagnostic).unwrap();
}
