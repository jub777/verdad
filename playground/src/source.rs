use eframe::epaint::ahash::HashMap;
use egui::text_edit::CCursorRange;
use egui::{text::LayoutJob, Galley};
use egui::{Color32, Stroke};
use std::sync::Arc;

use verdad::{Symbol, ThinTokenKind};

pub fn source_layouter(ui: &egui::Ui, source: &str, _: f32) -> Arc<Galley> {
    let span_to_color = error_span_to_color(source);
    let mut job = LayoutJob::default();
    for token in verdad::str_to_tokens(source) {
        job.append(
            &source[token.span.clone()],
            0.0,
            egui::TextFormat {
                font_id: egui::FontId::monospace(20.0),
                color: token_kind_to_color(&token.kind),
                underline: match span_to_color.get(&token.span) {
                    Some(color) => Stroke::new(2.0, *color),
                    None => Stroke::NONE,
                },
                ..Default::default()
            },
        );
    }
    ui.fonts(|f| f.layout_job(job))
}

fn error_span_to_color(source: &str) -> HashMap<verdad::Span, Color32> {
    match verdad::check(source) {
        Ok(warnings) => warnings
            .iter()
            .map(|e| (e.span(), Color32::YELLOW))
            .collect(),
        Err(error) => [(error.span(), Color32::RED)].into_iter().collect(),
    }
}

fn token_kind_to_color(kind: &ThinTokenKind) -> Color32 {
    match kind {
        ThinTokenKind::Symbol(
            Symbol::OpenBrace | Symbol::CloseBrace | Symbol::OpenParen | Symbol::CloseParen,
        ) => Color32::YELLOW,
        ThinTokenKind::Symbol(Symbol::Comma | Symbol::Colon | Symbol::Arrow)
        | ThinTokenKind::Unknown => Color32::WHITE,
        ThinTokenKind::Keyword(_) => Color32::LIGHT_RED,
        ThinTokenKind::Name => Color32::LIGHT_BLUE,
        ThinTokenKind::EndOfFile | ThinTokenKind::Whitespace => Color32::WHITE,
    }
}

pub fn format_source(
    unformatted: &str,
    old_cursor: CCursorRange,
) -> Option<(String, CCursorRange)> {
    let formatted = verdad::format_source(unformatted).ok()?;
    let new_cursor = updated_cursor_range(unformatted, old_cursor, &formatted);
    Some((formatted, new_cursor))
}

fn updated_cursor_range(
    unformatted: &str,
    old_cursor: CCursorRange,
    formatted: &str,
) -> CCursorRange {
    let mut new_cursor = old_cursor;
    new_cursor.primary.index =
        updated_cursor_position(unformatted, formatted, old_cursor.primary.index);
    new_cursor.secondary.index =
        updated_cursor_position(unformatted, formatted, old_cursor.secondary.index);
    new_cursor
}

fn updated_cursor_position(unformatted: &str, formatted: &str, old_cursor: usize) -> usize {
    let differences = diff::chars(unformatted, formatted);

    let mut new_cursor: usize = 0;
    let mut steps_through_unformatted: usize = 0;

    for difference in differences {
        if steps_through_unformatted < old_cursor {
            match difference {
                diff::Result::Left(_) => steps_through_unformatted += 1,
                diff::Result::Both(_, _) => {
                    steps_through_unformatted += 1;
                    new_cursor += 1;
                }
                diff::Result::Right(_) => new_cursor += 1,
            }
        } else {
            break;
        }
    }

    new_cursor
}

#[cfg(test)]
mod test {
    use super::*;
    use egui::text::CCursor;
    use pretty_assertions::assert_eq;

    #[test]
    fn update_cursor_position_trivial() {
        test_cursor(
            "^ind Bool {True, False}",
            "^ind Bool {\n    True,\n    False,\n}\n",
        );
    }

    #[test]
    fn update_cursor_position_inductive_middle() {
        test_cursor(
            "ind Bool {True^, False}",
            "ind Bool {\n    True^,\n    False,\n}\n",
        );
    }

    #[test]
    fn update_cursor_position_inductive_end() {
        test_cursor(
            "ind Bool {True, False}^",
            "ind Bool {\n    True,\n    False,\n}^\n",
        );
    }

    #[test]
    fn update_cursor_position_inductive_mid_word() {
        test_cursor(
            "ind Bool {True, Fa^lse}",
            "ind Bool {\n    True,\n    Fa^lse,\n}\n",
        );
    }

    #[test]
    #[should_panic]
    fn update_cursor_position_inductive_end_should_fail() {
        test_cursor(
            "ind Bool {True, False}^",
            "ind Bool {\n    True^,\n    False,\n}\n",
        );
    }

    #[test]
    fn update_cursor_when_removing_useless_parenthesis() {
        test_cursor(
            "ind Int {Zero()^, Inc(a: Int)}",
            "ind Int {\n    Zero^,\n    Inc(a: Int),\n}\n",
        );
    }

    fn test_cursor(before_with_cursor: &str, expected_with_cursor: &str) {
        let (source_before, cursor_before) = extract_cursor(before_with_cursor);
        let (source_expected, expected_cursor) = extract_cursor(expected_with_cursor);

        assert_eq!(
            format_source(&source_expected, expected_cursor).unwrap().0,
            source_expected,
            "Second argument to helper function was not a correctly formatted string"
        );

        let (source_after, cursor_after) = format_source(&source_before, cursor_before).unwrap();
        let after_with_cursor = insert_cursor(source_after, cursor_after);

        assert_eq!(
            after_with_cursor, expected_with_cursor,
            "After formatting cursor is not in correct position"
        );
    }

    fn extract_cursor(source_with_cursor: &str) -> (String, CCursorRange) {
        let cursor = source_with_cursor
            .chars()
            .position(|c| c == '^')
            .expect("Missing cursor char `^`");
        let source = source_with_cursor.chars().filter(|c| *c != '^').collect();
        let cursor_range = CCursorRange::one(CCursor::new(cursor));
        (source, cursor_range)
    }

    fn insert_cursor(mut source: String, cursor: CCursorRange) -> String {
        source.insert(cursor.primary.index, '^');
        source
    }
}
