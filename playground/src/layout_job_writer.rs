use codespan_reporting::term::termcolor::{Color, ColorSpec, WriteColor};
use egui::{text::LayoutJob, Color32, FontId, Stroke, TextFormat};

pub struct LayoutJobWriter<'j> {
    job: &'j mut LayoutJob,
    color_spec: ColorSpec,
    font: FontId,
}

impl<'j> LayoutJobWriter<'j> {
    pub fn new(job: &'j mut LayoutJob, font: FontId) -> Self {
        Self {
            job,
            font,
            color_spec: ColorSpec::default(),
        }
    }
}

impl<'j> std::io::Write for LayoutJobWriter<'j> {
    #[inline]
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let color = color_to_color32(self.color_spec.fg().unwrap_or(&Color::White));
        let underline = match self.color_spec.underline() {
            true => Stroke::new(2.0, Color32::YELLOW),
            false => Stroke::NONE,
        };
        self.job.append(
            std::str::from_utf8(buf).unwrap(),
            0.0,
            TextFormat {
                font_id: self.font.clone(),
                underline,
                color,
                ..Default::default()
            },
        );
        Ok(buf.len())
    }

    #[inline]
    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl<'j> WriteColor for LayoutJobWriter<'j> {
    fn supports_color(&self) -> bool {
        true
    }

    #[inline]
    fn set_color(&mut self, color_spec: &ColorSpec) -> std::io::Result<()> {
        self.color_spec = color_spec.clone();
        Ok(())
    }

    #[inline]
    fn reset(&mut self) -> std::io::Result<()> {
        self.color_spec = ColorSpec::default();
        Ok(())
    }

    #[inline]
    fn is_synchronous(&self) -> bool {
        false
    }
}

fn color_to_color32(color: &Color) -> Color32 {
    match color {
        Color::Black => Color32::BLACK,
        Color::Blue => Color32::BLUE,
        Color::Green => Color32::GREEN,
        Color::Red => Color32::RED,
        Color::Cyan => Color32::LIGHT_BLUE,
        Color::Magenta => Color32::DARK_BLUE,
        Color::Yellow => Color32::YELLOW,
        Color::White => Color32::WHITE,
        Color::Ansi256(1) => Color32::RED,
        Color::Ansi256(0) => Color32::BLACK,
        Color::Ansi256(2) => Color32::GREEN,
        Color::Ansi256(3) => Color32::YELLOW,
        Color::Ansi256(4) => Color32::BLUE,
        Color::Ansi256(5) => Color32::DARK_BLUE,
        Color::Ansi256(6) => Color32::LIGHT_BLUE,
        Color::Ansi256(7) => Color32::WHITE,
        Color::Ansi256(8) => Color32::BLACK,
        Color::Ansi256(9) => Color32::RED,
        Color::Ansi256(10) => Color32::GREEN,
        Color::Ansi256(11) => Color32::YELLOW,
        Color::Ansi256(12) => Color32::BLUE,
        Color::Ansi256(13) => Color32::DARK_BLUE,
        Color::Ansi256(14) => Color32::LIGHT_BLUE,
        Color::Ansi256(15) => Color32::WHITE,
        Color::Rgb(r, g, b) => Color32::from_rgb(*r, *g, *b),
        &_ => panic!("Unexpected color"),
    }
}
