use codespan_reporting::term::ColorArg;
use structopt::StructOpt;

mod color;
mod error;
mod format;

fn main() {
    let result = match Command::from_args() {
        Command::Check(arguments) => check(arguments),
        Command::Format(arguments) => format::format(arguments),
        Command::Color(arguments) => color::color(arguments),
    };
    exit_with_return_code(result)
}

fn exit_with_return_code(result: Result<(), ()>) {
    match result {
        Ok(()) => std::process::exit(0),
        Err(()) => std::process::exit(1),
    }
}

#[derive(StructOpt)]
enum Command {
    Check(CheckArgs),
    Format(format::FormatArgs),
    Color(color::ColorArgs),
}

#[derive(StructOpt)]
#[structopt(about = "Checks a verdad source file and prints any errors or warnings")]
struct CheckArgs {
    #[structopt(
        long = "color",
        short,
        default_value = "auto",
        possible_values = ColorArg::VARIANTS,
        case_insensitive = true,
    )]
    pub color: ColorArg,

    path: Option<std::path::PathBuf>,
}

fn check(arguments: CheckArgs) -> Result<(), ()> {
    let source = color::get_source(&arguments.path);
    let path = arguments
        .path
        .as_ref()
        .map(|path| path.to_str().unwrap())
        .unwrap_or("(stdin)");
    match verdad::check(&source) {
        Ok(warnings) if !warnings.is_empty() => {
            error::print_warnings(path, &source, warnings, arguments.color.into());
            Err(())
        }
        Err(error) => {
            error::print_error(path, &source, error, arguments.color.into());
            Err(())
        }
        _ => Ok(()),
    }
}
