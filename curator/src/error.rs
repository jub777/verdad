use codespan_reporting::files::SimpleFiles;
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};
use std::io::Write;
use verdad::{BoxedError, Error};

pub fn print_warnings(path: &str, source: &str, warnings: Vec<BoxedError>, color: ColorChoice) {
    for warning in warnings.iter() {
        print_error_diagnostic(path, source, warning.as_ref(), color);
    }

    writeln!(
        std::io::stderr().lock(),
        "Generated {} warning(s)",
        warnings.len()
    )
    .unwrap();
}

pub fn print_error(path: &str, source: &str, error: BoxedError, color: ColorChoice) {
    print_error_diagnostic(path, source, error.as_ref(), color);
}

fn print_error_diagnostic(path: &str, source: &str, error: &dyn Error, color: ColorChoice) {
    let mut files: SimpleFiles<&str, &str> = SimpleFiles::new();
    let file_id = files.add(path, source);
    let mut writer = StandardStream::stderr(color);
    let config = codespan_reporting::term::Config::default();
    let diagnostic = error.diagnostic(file_id);
    codespan_reporting::term::emit(&mut writer, &config, &files, &diagnostic).unwrap();
}
