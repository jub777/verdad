use codespan_reporting::term::termcolor::ColorChoice;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Auto-formats verdad source files (or current folder)")]
pub struct FormatArgs {
    #[structopt(long, short)]
    emit: bool,
    path: Option<std::path::PathBuf>,
}

pub fn format(args: FormatArgs) -> Result<(), ()> {
    if let Some(path) = args.path {
        format_file(&path, args.emit)
    } else {
        for path in all_verdad_files() {
            format_file(&path, args.emit)?;
        }
        Ok(())
    }
}

fn format_file(path: &std::path::Path, emit: bool) -> Result<(), ()> {
    let unformatted = std::fs::read_to_string(path).unwrap();
    let formatted = verdad::str_to_cst(&unformatted)
        .map(|cst| verdad::cst_to_str(&cst))
        .map_err(|error| {
            super::error::print_error(
                path.to_str().unwrap(),
                &unformatted,
                error,
                ColorChoice::Auto,
            );
        })?;
    if emit {
        print!("{}", formatted);
    } else {
        std::fs::write(path, formatted).unwrap();
    }
    Ok(())
}

fn all_verdad_files() -> impl Iterator<Item = std::path::PathBuf> {
    walkdir::WalkDir::new(".")
        .into_iter()
        .filter_map(|v| v.ok())
        .filter(|e| e.file_name().to_str().unwrap().ends_with(".vd"))
        .map(|entry| entry.into_path())
}
