use codespan_reporting::term::termcolor::{
    Color, ColorChoice, ColorSpec, StandardStream, WriteColor,
};
use structopt::StructOpt;
use verdad::{Symbol, ThinTokenKind};

use std::io::{Read, Write};

#[derive(StructOpt)]
#[structopt(about = "Reads a verdad source file and prints it with color to stdout")]
pub struct ColorArgs {
    path: Option<std::path::PathBuf>,
}

pub fn color(arguments: ColorArgs) -> Result<(), ()> {
    let source = get_source(&arguments.path);
    let mut writer = StandardStream::stdout(ColorChoice::Auto);
    for token in verdad::str_to_tokens(&source) {
        set_color(&mut writer, token_kind_to_color(&token.kind));
        write!(writer.lock(), "{}", &source[token.span.clone()]).unwrap();
    }
    Ok(())
}

pub fn get_source(path: &Option<std::path::PathBuf>) -> String {
    match path {
        Some(path) => std::fs::read_to_string(path).unwrap(),
        None => {
            let mut string = String::new();
            std::io::stdin().lock().read_to_string(&mut string).unwrap();
            string
        }
    }
}

fn set_color(writer: &mut StandardStream, color: Option<Color>) {
    let mut spec = ColorSpec::default();
    spec.set_fg(color);
    writer.set_color(&spec).unwrap();
}

fn token_kind_to_color(kind: &ThinTokenKind) -> Option<Color> {
    match kind {
        ThinTokenKind::Symbol(
            Symbol::OpenBrace | Symbol::CloseBrace | Symbol::OpenParen | Symbol::CloseParen,
        ) => Some(Color::Yellow),
        ThinTokenKind::Symbol(Symbol::Comma | Symbol::Colon | Symbol::Arrow)
        | ThinTokenKind::Unknown => Some(Color::White),
        ThinTokenKind::Keyword(_) => Some(Color::Magenta),
        ThinTokenKind::Name => Some(Color::Cyan),
        ThinTokenKind::EndOfFile | ThinTokenKind::Whitespace => None,
    }
}
