use pretty_assertions::assert_eq;

#[test]
fn test_examples_formatted_correctly() {
    for entry in all_example_files() {
        let unformatted = std::fs::read_to_string(entry.path()).unwrap();
        if let Ok(formatted) = verdad::format_source(&unformatted) {
            assert_eq!(
                unformatted,
                formatted,
                "Example file {} is not correctly formatted",
                entry.path().to_str().unwrap()
            );
        }
    }
}

#[test]
fn test_examples_check_output() {
    insta::glob!("../../", "examples/**/*.vd", |path| {
        println!("checking {:?}", path);
        let source = std::fs::read_to_string(path).unwrap();
        let errors = match verdad::check(&source) {
            Ok(errors) => errors
                .into_iter()
                .map(|error| {
                    verdad::error_diagnostic_to_string(
                        path.file_name().unwrap().to_str().unwrap(),
                        &source,
                        error.as_ref(),
                    )
                })
                .fold(String::new(), |document, suffix| document + &suffix + "\n"),
            Err(error) => verdad::error_diagnostic_to_string(
                path.file_name().unwrap().to_str().unwrap(),
                &source,
                error.as_ref(),
            ),
        };
        insta::assert_snapshot!(errors);
    });
}

fn all_example_files() -> impl Iterator<Item = walkdir::DirEntry> {
    walkdir::WalkDir::new("../examples/")
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| !e.file_type().is_dir())
}
