mod wrong_case;

use crate::cst::{Cst, Visitor};
use crate::error::BoxedError;

pub fn all_warnings(cst: &Cst) -> Vec<BoxedError> {
    std::iter::empty()
        .chain(wrong_case::Visitor::new().visit(cst).unwrap())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    pub fn str_to_warnings(source: &str) -> Vec<BoxedError> {
        let cst = crate::str_to_cst(source).unwrap();
        all_warnings(&cst)
    }

    #[test]
    fn no_warnings_on_empty_cst() {
        assert!(str_to_warnings("").is_empty());
    }
}
