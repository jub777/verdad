use crate::cst::{Constructor, Function, Ident};
use crate::error::{BoxedError, Diagnostic, Label};
use crate::{cst::InductiveDefinition, token::Span};

pub struct Visitor {
    pub errors: Vec<BoxedError>,
}

impl Visitor {
    pub fn new() -> Self {
        Self { errors: Vec::new() }
    }

    fn check_ident(&mut self, ident: &Ident, expected_case: Case) {
        let name = ident.symbol.as_str();
        if !expected_case.check(name) {
            self.errors.push(Box::from(NameWrongCase {
                span: ident.span.clone(),
                name: name.to_string(),
                suggestion: expected_case.convert(name),
                expected_case,
            }))
        }
    }
}

impl crate::cst::Visitor for Visitor {
    type Output = Vec<BoxedError>;
    type Error = ();

    fn visit_inductive(&mut self, inductive: &InductiveDefinition) -> Result<(), ()> {
        self.check_ident(&inductive.name, Case::Pascal);
        crate::cst::walk_inductive(self, inductive)?;
        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &Constructor) -> Result<(), ()> {
        self.check_ident(&constructor.name, Case::Pascal);
        Ok(())
    }

    fn visit_function(&mut self, function: &Function) -> Result<(), ()> {
        self.check_ident(&function.name, Case::Snake);
        crate::cst::walk_function_arguments(self, function)?;
        Ok(())
    }

    fn visit_argument(&mut self, argument: &crate::cst::Argument) -> Result<(), ()> {
        self.check_ident(&argument.name, Case::Snake);
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(self.errors)
    }
}

#[derive(Debug, PartialEq)]
pub struct NameWrongCase {
    name: String,
    span: Span,
    expected_case: Case,
    suggestion: String,
}

impl crate::Error for NameWrongCase {
    fn diagnostic(&self, file_id: usize) -> Diagnostic {
        Diagnostic::warning()
            .with_message(format!(
                "wrong case: `{}` should have a {} case name",
                self.name, self.expected_case
            ))
            .with_labels(vec![Label::primary(file_id, self.span.clone())
                .with_message(format!(
                    "help: convert to {} case (note capitalisation): `{}`",
                    self.expected_case, self.suggestion
                ))])
    }

    fn span(&self) -> Span {
        self.span.clone()
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[derive(Debug, PartialEq)]
enum Case {
    Pascal,
    Snake,
}

impl Case {
    fn check(&self, string: &str) -> bool {
        match self {
            Self::Pascal => inflections::case::is_pascal_case(string),
            Self::Snake => inflections::case::is_snake_case(string),
        }
    }

    fn convert(&self, string: &str) -> String {
        match self {
            Self::Pascal => inflections::case::to_pascal_case(string),
            Self::Snake => inflections::case::to_snake_case(string),
        }
    }
}

impl std::fmt::Display for Case {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Pascal => write!(f, "pascal"),
            Self::Snake => write!(f, "snake"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::error::DowncastErrVec;
    use crate::lint::test::str_to_warnings;

    #[test]
    fn no_wrong_case_warnings_properly_formatted() {
        assert!(str_to_warnings("ind Never {}").is_empty());
        assert!(str_to_warnings("ind Bool {True,False}").is_empty());
        assert!(str_to_warnings("fn false() -> Bool {False}").is_empty());
        assert!(str_to_warnings("fn false(a: Bool, a_thing: Bool) -> Bool {False}").is_empty());
    }

    #[test]
    fn warnings_on_induction_definition_with_wrong_cases() {
        assert_eq!(
            str_to_warnings("ind bool {true,false}").downcast_err_vec::<NameWrongCase>(),
            vec![
                &NameWrongCase {
                    name: "bool".to_string(),
                    span: 4..8,
                    expected_case: Case::Pascal,
                    suggestion: "Bool".to_string()
                },
                &NameWrongCase {
                    name: "true".to_string(),
                    span: 10..14,
                    expected_case: Case::Pascal,
                    suggestion: "True".to_string()
                },
                &NameWrongCase {
                    name: "false".to_string(),
                    span: 15..20,
                    expected_case: Case::Pascal,
                    suggestion: "False".to_string()
                }
            ]
        );
    }

    #[test]
    fn warnings_on_function_definition_with_wrong_cases() {
        assert_eq!(
            str_to_warnings("fn False(A: Bool, aThing: Bool) -> Bool {False}")
                .downcast_err_vec::<NameWrongCase>(),
            vec![
                &NameWrongCase {
                    name: "False".to_string(),
                    span: 3..8,
                    expected_case: Case::Snake,
                    suggestion: "false".to_string()
                },
                &NameWrongCase {
                    name: "A".to_string(),
                    span: 9..10,
                    expected_case: Case::Snake,
                    suggestion: "a".to_string()
                },
                &NameWrongCase {
                    name: "aThing".to_string(),
                    span: 18..24,
                    expected_case: Case::Snake,
                    suggestion: "a_thing".to_string()
                }
            ]
        );
    }
}
