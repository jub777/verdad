use super::reservation::ReservedNames;
use crate::ast::{InductiveId, SignatureNamespace};
use crate::cst;
use crate::error::BoxedError;

pub struct SignatureBuilder<'a> {
    reservations: &'a ReservedNames,
    signatures: SignatureNamespace,
    inductive_id: Option<InductiveId>,
}

impl<'a> SignatureBuilder<'a> {
    pub fn new(reservations: &'a ReservedNames) -> Self {
        Self {
            reservations,
            signatures: SignatureNamespace::default(),
            inductive_id: None,
        }
    }
}

impl<'a> cst::Visitor for SignatureBuilder<'a> {
    type Output = SignatureNamespace;
    type Error = BoxedError;

    fn visit_inductive(&mut self, inductive: &cst::InductiveDefinition) -> Result<(), Self::Error> {
        let expected_id = self
            .reservations
            .try_get(&inductive.name)
            .unwrap()
            .id
            .unwrap_inductive();
        let inductive_id = self
            .signatures
            .add_inductive(super::inductive::cst_to_inductive(
                self.reservations,
                inductive,
            )?);
        assert_eq!(expected_id, inductive_id);
        self.inductive_id = Some(inductive_id);
        crate::cst::walk_inductive(self, inductive)?;
        self.inductive_id = None;

        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &cst::Constructor) -> Result<(), Self::Error> {
        let constructor = super::constructor::cst_to_constructor_signature(
            self.reservations,
            self.inductive_id.unwrap(),
            constructor,
        )?;
        self.signatures.add_constructor(constructor);
        Ok(())
    }

    fn visit_function(&mut self, function: &cst::Function) -> Result<(), Self::Error> {
        self.signatures
            .add_function(super::function::cst_to_function_signature(
                self.reservations,
                function,
            )?);
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(self.signatures)
    }
}
