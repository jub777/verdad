use super::reservation::{DuplicateNameError, NotFoundError};
use crate::ast::{Argument, InductiveId, VariableId};
use crate::{cst, BoxedError};

pub trait Scope {
    fn get(&self, name: &cst::Ident) -> Option<&Variable>;

    fn try_get(&self, name: &cst::Ident) -> Result<&Variable, BoxedError> {
        self.get(name).ok_or_else(|| -> BoxedError {
            Box::new(NotFoundError {
                ident: name.clone(),
            })
        })
    }

    fn new_arm(&mut self) -> FrameScope;

    fn add_variable(
        &mut self,
        name: &cst::Ident,
        inductive_id: InductiveId,
    ) -> Result<VariableId, BoxedError>;
}

pub struct StackScope {
    variables: Vec<Variable>,
}

impl StackScope {
    pub fn new_function(arguments: &[Argument]) -> Result<Self, BoxedError> {
        let mut scope = Self {
            variables: Vec::with_capacity(arguments.len()),
        };
        for argument in arguments.iter() {
            scope.add_variable(&argument.name, argument.class.inductive_id)?;
        }
        Ok(scope)
    }
}

impl Scope for StackScope {
    fn get(&self, name: &cst::Ident) -> Option<&Variable> {
        self.variables.iter().find(|a| a.name.symbol == name.symbol)
    }

    fn new_arm(&mut self) -> FrameScope {
        FrameScope {
            frame: self.variables.len(),
            stack: self,
        }
    }

    fn add_variable(
        &mut self,
        name: &cst::Ident,
        inductive_id: InductiveId,
    ) -> Result<VariableId, BoxedError> {
        if let Some(variable) = self.variables.iter().find(|a| name.symbol == a.name.symbol) {
            return Err(Box::new(DuplicateNameError {
                original: variable.name.span.clone(),
                duplicate: name.clone(),
            }));
        }
        let variable_id = VariableId(self.variables.len());
        self.variables.push(Variable {
            name: name.clone(),
            variable_id,
            inductive_id,
        });
        Ok(variable_id)
    }
}

pub struct FrameScope<'a> {
    stack: &'a mut StackScope,
    frame: usize,
}

impl<'a> Scope for FrameScope<'a> {
    fn get(&self, name: &cst::Ident) -> Option<&Variable> {
        self.stack.get(name)
    }

    fn new_arm(&mut self) -> FrameScope {
        self.stack.new_arm()
    }

    fn add_variable(
        &mut self,
        name: &cst::Ident,
        inductive_id: InductiveId,
    ) -> Result<VariableId, BoxedError> {
        self.stack.add_variable(name, inductive_id)
    }
}

impl<'a> Drop for FrameScope<'a> {
    fn drop(&mut self) {
        self.stack.variables.truncate(self.frame);
    }
}

#[derive(Debug)]
pub struct Variable {
    pub variable_id: VariableId,
    pub inductive_id: InductiveId,
    name: cst::Ident,
}
