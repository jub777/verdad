use super::argument::cst_to_arguments;
use super::reservation::ReservedNames;
use super::scope::StackScope;
use crate::{ast::Inductive, cst, BoxedError};

pub fn cst_to_inductive(
    globals: &ReservedNames,
    inductive: &cst::InductiveDefinition,
) -> Result<Inductive, BoxedError> {
    let arguments = cst_to_arguments(globals, &inductive.arguments)?;
    StackScope::new_function(&arguments)?;
    Ok(Inductive {
        name: inductive.name.clone(),
        arguments,
        constructor_ids: inductive
            .constructors
            .iter()
            .map(|constructor| {
                globals
                    .try_get(&constructor.name)
                    .map(|id| id.id.unwrap_constructor())
            })
            .collect::<Result<Vec<_>, _>>()?,
    })
}
#[cfg(test)]
mod test {
    use crate::{
        abstractor::{reservation::DuplicateNameError, test::str_to_ast},
        ast::{Argument, Inductive, InductiveId, TypeRef},
        cst::Ident,
    };

    #[test]
    fn inductive_definition_with_arguments() {
        assert_eq!(
            str_to_ast("ind Foo{} ind Bar(a: Foo){}")
                .unwrap()
                .definitions[InductiveId(1)],
            Inductive {
                name: Ident::new(14, "Bar"),
                arguments: vec![Argument {
                    name: Ident::new(18, "a"),
                    class: TypeRef {
                        inductive_id: InductiveId(0),
                        ident: Ident::new(21, "Foo")
                    }
                }],
                constructor_ids: vec![]
            },
        );
    }

    #[test]
    fn duplicate_inductive_arguments_errors() {
        assert_eq!(
            str_to_ast("ind Foo{} ind Bar(a: Foo, a: Foo){}").unwrap_err(),
            DuplicateNameError {
                original: 18..19,
                duplicate: Ident::new(26, "a")
            }
        );
    }
}
