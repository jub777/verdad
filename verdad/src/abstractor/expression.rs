use super::reservation::ReservedNames;
use super::scope::{Scope, Variable};
use crate::ast::{
    Argument, Arm, BindConstructor, BindVariable, Call, ConstructorSignature, Expression, Match,
    SignatureNamespace, VariableRef,
};
use crate::error::{BoxedError, Diagnostic, Error, Label};
use crate::{cst, Span};

pub fn cst_to_expression(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    expression: &cst::Expression,
) -> Result<Expression, BoxedError> {
    match expression {
        cst::Expression::Call(call) => cst_call_to_expression(globals, signatures, locals, call),
        cst::Expression::Match(match_definition) => {
            cst_to_match(globals, signatures, locals, match_definition).map(Expression::Match)
        }
    }
}

fn cst_call_to_expression(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    call: &cst::Call,
) -> Result<Expression, BoxedError> {
    if let Some(variable) = locals.get(&call.name).filter(|_| call.arguments.is_none()) {
        Ok(Expression::VariableRef(variable_to_variable_ref(
            &call.name, variable,
        )))
    } else {
        cst_to_call(globals, signatures, locals, call).map(Expression::ConstructorCall)
    }
}

pub fn cst_to_call(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    call: &cst::Call,
) -> Result<Call, BoxedError> {
    Ok(Call {
        ident: call.name.clone(),
        namespace_id: globals.try_get(&call.name)?.id.clone(),
        arguments: call
            .arguments
            .as_ref()
            .map(|arguments| {
                arguments
                    .iter()
                    .map(|argument| cst_to_expression(globals, signatures, locals, argument))
                    .collect::<Result<Vec<_>, _>>()
            })
            .unwrap_or_else(|| Ok(Vec::new()))?,
    })
}

fn cst_to_match(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    match_definition: &cst::Match,
) -> Result<Match, BoxedError> {
    Ok(Match {
        argument: cst_to_variable_ref(locals, &match_definition.argument)?,
        arms: match_definition
            .arms
            .iter()
            .map(|arm| cst_to_arm(globals, signatures, locals, arm))
            .collect::<Result<_, _>>()?,
        span: match_definition.span.clone(),
    })
}

fn cst_to_variable_ref(locals: &dyn Scope, ident: &cst::Ident) -> Result<VariableRef, BoxedError> {
    Ok(variable_to_variable_ref(ident, locals.try_get(ident)?))
}

fn variable_to_variable_ref(ident: &cst::Ident, variable: &Variable) -> VariableRef {
    VariableRef {
        variable_id: variable.variable_id,
        ident: ident.clone(),
        inductive_id: variable.inductive_id,
    }
}

fn cst_to_arm(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    arm: &cst::Arm,
) -> Result<Arm, BoxedError> {
    let mut locals = locals.new_arm();
    Ok(Arm {
        pattern: cst_to_pattern(globals, signatures, &mut locals, &arm.pattern)?,
        expression: cst_to_expression(globals, signatures, &mut locals, &arm.expression)?,
    })
}

fn cst_to_pattern(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    locals: &mut dyn Scope,
    pattern: &cst::Pattern,
) -> Result<BindConstructor, BoxedError> {
    let constructor_id = globals.try_get(&pattern.name)?.id.unwrap_constructor();
    let constructor = &signatures[constructor_id];
    check_patterns_and_bindings_length_match(pattern, constructor)?;
    Ok(BindConstructor {
        ident: pattern.name.clone(),
        constructor_id,
        sub_patterns: cst_to_sub_patterns(locals, &pattern.bindings, &constructor.arguments)?,
    })
}

fn cst_to_sub_patterns(
    locals: &mut dyn Scope,
    sub_patterns: &[cst::Ident],
    arguments: &[Argument],
) -> Result<Vec<BindVariable>, BoxedError> {
    sub_patterns
        .iter()
        .zip(arguments.iter())
        .map(|(ident, argument)| {
            Ok(BindVariable {
                variable_id: locals.add_variable(ident, argument.class.inductive_id)?,
                ident: ident.clone(),
            })
        })
        .collect()
}

fn check_patterns_and_bindings_length_match(
    pattern: &cst::Pattern,
    constructor: &ConstructorSignature,
) -> Result<(), BoxedError> {
    let expected = constructor.arguments.len();
    let found = pattern.bindings.len();
    if found == expected {
        Ok(())
    } else {
        Err(BoxedError::from(IncorrectBindingCount {
            expected,
            found,
            pattern: pattern.name.span.clone(),
            constructor: constructor.name.span.clone(),
        }))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct IncorrectBindingCount {
    expected: usize,
    found: usize,
    pattern: Span,
    constructor: Span,
}

impl Error for IncorrectBindingCount {
    fn span(&self) -> Span {
        self.pattern.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "this pattern has {} bindings, but the corresponding constructor has {} arguments",
                self.found, self.expected
            ))
            .with_labels(vec![
                Label::primary(file_id, self.pattern.clone()).with_message(format!(
                    "expected {} binding, found {}",
                    self.expected, self.found
                )),
                Label::primary(file_id, self.constructor.clone())
                    .with_message(format!("constructor has {} arguments", self.expected)),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::abstractor::reservation::NotFoundError;
    use crate::abstractor::test::str_to_ast;
    use crate::ast::{
        Arm, BindVariable, ConstructorId, FunctionId, InductiveId, NamespaceId, VariableId,
        VariableRef,
    };
    use crate::cst::Ident;

    #[test]
    fn call_with_arguments_cannot_be_variable_ref() {
        assert_eq!(
            str_to_ast("ind Never{} fn foo(a: Never) -> Never { a() }").unwrap_err(),
            NotFoundError {
                ident: Ident::new(40, "a")
            }
        );
    }

    #[test]
    fn simple_variable_reference() {
        assert_eq!(
            str_to_ast("ind Never{} fn foo(a: Never) -> Never { a }")
                .unwrap()
                .definitions[FunctionId(1)]
            .body,
            Expression::VariableRef(VariableRef {
                variable_id: VariableId(0),
                ident: Ident::new(40, "a"),
                inductive_id: InductiveId(0)
            })
        );
    }

    #[test]
    fn simple_pattern_bind() {
        let source = "ind X {Y} ind Foo{Bar(a: X)} fn foo(b: Foo) -> X {match b{Bar(c)->Y}}";

        assert_eq!(
            str_to_ast(source).unwrap().definitions[FunctionId(4)].body,
            Expression::Match(Match {
                argument: VariableRef {
                    ident: Ident::new(56, "b"),
                    variable_id: VariableId(0),
                    inductive_id: InductiveId(2),
                },
                arms: vec![Arm {
                    pattern: BindConstructor {
                        ident: Ident::new(58, "Bar"),
                        constructor_id: ConstructorId(3),
                        sub_patterns: vec![BindVariable {
                            variable_id: VariableId(1),
                            ident: Ident::new(62, "c"),
                        }],
                    },
                    expression: Expression::ConstructorCall(Call {
                        ident: Ident::new(66, "Y"),
                        namespace_id: NamespaceId::Constructor(ConstructorId(1)),
                        arguments: vec![]
                    })
                }],
                span: (50..68)
            })
        );
    }

    #[test]
    fn nested_pattern_bind() {
        let source = "ind Never {} ind Foo{Bar(a: Never)} fn foo(b: Foo) -> Never {match b{Bar(c)->match c{}}}";

        assert_eq!(
            str_to_ast(source).unwrap().definitions[FunctionId(3)].body,
            Expression::Match(Match {
                argument: VariableRef {
                    ident: Ident::new(67, "b"),
                    variable_id: VariableId(0),
                    inductive_id: InductiveId(1),
                },
                arms: vec![Arm {
                    pattern: BindConstructor {
                        ident: Ident::new(69, "Bar"),
                        constructor_id: ConstructorId(2),
                        sub_patterns: vec![BindVariable {
                            variable_id: VariableId(1),
                            ident: Ident::new(73, "c"),
                        }],
                    },
                    expression: Expression::Match(Match {
                        argument: VariableRef {
                            ident: Ident::new(83, "c"),
                            variable_id: VariableId(1),
                            inductive_id: InductiveId(0),
                        },
                        arms: vec![],
                        span: (77..86)
                    })
                }],
                span: (61..87)
            })
        );
    }

    // TODO
    // #[test]
    // fn expression_type_does_not_match_return_because_object_is_invalid_call() {
    //     assert_eq!(
    //         str_to_ast("ind Never{} fn invalid() -> Never {Never}").unwrap_err(),
    //         ConstructorNameError { span: 35..40 }
    //     );
    // }

    #[test]
    fn trivial_call_expression() {
        let source = "ind Bool{True,False} fn false() -> Bool {False}";

        assert_eq!(
            str_to_ast(source).unwrap().definitions[FunctionId(3)].body,
            Expression::ConstructorCall(Call {
                ident: Ident::new(41, "False"),
                namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                arguments: vec![]
            })
        );
    }

    #[test]
    fn call_expression_with_argument() {
        let source = "ind Int{Zero,Inc(n: Int)} fn one() -> Int {Inc(Zero)}";

        assert_eq!(
            str_to_ast(source).unwrap().definitions[FunctionId(3)].body,
            Expression::ConstructorCall(Call {
                ident: Ident::new(43, "Inc"),
                namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                arguments: vec![Expression::ConstructorCall(Call {
                    ident: Ident::new(47, "Zero"),
                    namespace_id: NamespaceId::Constructor(ConstructorId(1)),
                    arguments: vec![]
                })]
            })
        );
    }

    #[test]
    fn trivial_match_expression() {
        let source = "ind Never{} fn never(a: Never) -> Never {match a {}}";

        assert_eq!(
            str_to_ast(source).unwrap().definitions[FunctionId(1)].body,
            Expression::Match(Match {
                argument: VariableRef {
                    ident: Ident::new(47, "a"),
                    variable_id: VariableId(0),
                    inductive_id: InductiveId(0),
                },
                arms: Vec::new(),
                span: (41..51)
            })
        );
    }

    #[test]
    fn simple_two_arm_match_expression() {
        let source =
            "ind Bool{False,True} fn not(a: Bool) -> Bool {match a {False->True, True->False}}";

        let ast = str_to_ast(source).unwrap();
        assert_eq!(
            ast.definitions[FunctionId(3)].body,
            Expression::Match(Match {
                argument: VariableRef {
                    ident: Ident::new(52, "a"),
                    variable_id: VariableId(0),
                    inductive_id: InductiveId(0),
                },
                arms: vec![
                    Arm {
                        pattern: BindConstructor {
                            ident: Ident::new(55, "False"),
                            constructor_id: ConstructorId(1),
                            sub_patterns: vec![],
                        },
                        expression: Expression::ConstructorCall(Call {
                            ident: Ident::new(62, "True"),
                            namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                            arguments: vec![]
                        })
                    },
                    Arm {
                        pattern: BindConstructor {
                            ident: Ident::new(68, "True"),
                            constructor_id: ConstructorId(2),
                            sub_patterns: vec![],
                        },
                        expression: Expression::ConstructorCall(Call {
                            ident: Ident::new(74, "False"),
                            namespace_id: NamespaceId::Constructor(ConstructorId(1)),
                            arguments: vec![]
                        })
                    },
                ],
                span: (46..80)
            })
        );
    }

    #[test]
    fn correct_bindings_count_ok() {
        str_to_ast(
            "ind Foo{Bar(n: Foo)} ind Fizz{Buzz} fn foo(a: Foo) -> Fizz {match a {Bar(b)->Buzz}}",
        )
        .unwrap();
    }

    #[test]
    fn too_few_bindings_errors() {
        assert_eq!(
            str_to_ast(
                "ind Foo{Bar(n: Foo)} ind Fizz{Buzz} fn foo(a: Foo) -> Fizz {match a {Bar->Buzz}}"
            )
            .unwrap_err(),
            IncorrectBindingCount {
                expected: 1,
                found: 0,
                pattern: (69..72),
                constructor: (8..11)
            }
        );
    }

    #[test]
    fn too_many_bindings_errors() {
        assert_eq!(
            str_to_ast(
                "ind Foo{Bar(n: Foo)} ind Fizz{Buzz} fn foo(a: Foo) -> Fizz {match a {Bar(b,c)->Buzz}}"
            )
            .unwrap_err(),
            IncorrectBindingCount {
                expected: 1,
                found: 2,
                pattern: (69..72),
                constructor: (8..11)
            }
        );
    }
}
