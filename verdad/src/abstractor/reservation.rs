use std::collections::HashMap;

use crate::ast::{ConstructorId, FunctionId, InductiveId, NamespaceId};
use crate::cst::Ident;
use crate::error::{Diagnostic, Error, Label};
use crate::{cst, BoxedError};
use crate::{Span, StringId};

#[derive(Debug, PartialEq, Eq)]
pub struct ReservedNames(HashMap<StringId, Reservation>);

impl ReservedNames {
    pub fn try_get(&self, name: &cst::Ident) -> Result<&Reservation, BoxedError> {
        self.0.get(&name.symbol).ok_or_else(|| -> BoxedError {
            Box::new(NotFoundError {
                ident: name.clone(),
            })
        })
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Reservation {
    pub id: NamespaceId,
    pub span: Span,
}

pub struct ReservedNamesBuilder {
    reservations: ReservedNames,
}

impl ReservedNamesBuilder {
    pub fn new() -> Self {
        Self {
            reservations: ReservedNames(HashMap::new()),
        }
    }

    fn add_definition(&mut self, name: &cst::Ident, id: NamespaceId) -> Result<(), BoxedError> {
        if let Some(prev_entry_id) = self.reservations.0.insert(
            name.symbol,
            Reservation {
                id,
                span: name.span.clone(),
            },
        ) {
            return Err(Box::new(DuplicateNameError {
                original: prev_entry_id.span,
                duplicate: name.clone(),
            }));
        }
        Ok(())
    }
}

impl cst::Visitor for ReservedNamesBuilder {
    type Output = ReservedNames;
    type Error = BoxedError;

    fn visit_inductive(&mut self, inductive: &cst::InductiveDefinition) -> Result<(), Self::Error> {
        self.add_definition(
            &inductive.name,
            NamespaceId::Inductive(InductiveId(self.reservations.0.len())),
        )?;
        cst::walk_inductive(self, inductive)?;
        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &cst::Constructor) -> Result<(), Self::Error> {
        self.add_definition(
            &constructor.name,
            NamespaceId::Constructor(ConstructorId(self.reservations.0.len())),
        )?;
        Ok(())
    }

    fn visit_function(&mut self, function: &cst::Function) -> Result<(), Self::Error> {
        self.add_definition(
            &function.name,
            NamespaceId::Function(FunctionId(self.reservations.0.len())),
        )?;
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(self.reservations)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct DuplicateNameError {
    pub original: Span,
    pub duplicate: Ident,
}

impl Error for DuplicateNameError {
    fn span(&self) -> Span {
        self.duplicate.span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "the name `{}` is defined multiple times",
                self.duplicate.symbol
            ))
            .with_labels(vec![
                Label::primary(file_id, self.original.clone()).with_message(format!(
                    "previous definition of `{}` here",
                    self.duplicate.symbol
                )),
                Label::primary(file_id, self.duplicate.span.clone())
                    .with_message(format!("redefinition of `{}` here", self.duplicate.symbol)),
            ])
            .with_notes(vec![format!(
                "`{}` must be defined only once in the namespace of this module",
                self.duplicate.symbol
            )])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct NotFoundError {
    pub ident: Ident,
}

impl Error for NotFoundError {
    fn span(&self) -> Span {
        self.ident.span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "the name `{}` could not be found",
                self.ident.symbol
            ))
            .with_labels(vec![Label::primary(file_id, self.ident.span.clone())
                .with_message("name referenced here".to_string())])
            .with_notes(vec![format!(
                "`{}` might not have been declared or imported",
                self.ident.symbol
            )])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
pub mod test {
    use map_macro::map;

    use super::*;
    use crate::abstractor::test::str_to_ast;
    use crate::cst::Visitor;

    #[test]
    fn inductive_definition() {
        assert_eq!(
            str_to_definitions("ind Never{}").unwrap(),
            ReservedNames(map! {
                StringId::new("Never") => Reservation {
                    id: NamespaceId::Inductive(InductiveId(0)),
                    span: 4..9
                }
            })
        );
    }

    #[test]
    fn duplicated_inductive_names_error() {
        assert_eq!(
            str_to_definitions("ind Never{} ind Never{}").unwrap_err(),
            DuplicateNameError {
                original: 4..9,
                duplicate: Ident::new(16, "Never"),
            }
        )
    }

    #[test]
    fn multiple_inductive_names() {
        assert_eq!(
            str_to_definitions("ind Never{} ind Ever{}").unwrap(),
            ReservedNames(map! {
                StringId::new("Never") => Reservation {
                    id: NamespaceId::Inductive(InductiveId(0)),
                    span: 4..9
                },
                StringId::new("Ever") => Reservation {
                    id: NamespaceId::Inductive(InductiveId(1)),
                    span: 16..20
                }
            })
        );
    }

    #[test]
    fn inductive_constructor_and_function_definitions() {
        assert_eq!(
            str_to_definitions("ind Bool{False, True} fn not() -> Bool { False }").unwrap(),
            ReservedNames(map! {
                StringId::new("Bool") => Reservation {
                    id: NamespaceId::Inductive(InductiveId(0)),
                    span: 4..8
                },
                StringId::new("False") => Reservation {
                    id: NamespaceId::Constructor(ConstructorId(1)),
                    span: 9..14
                },
                StringId::new("True") => Reservation {
                    id: NamespaceId::Constructor(ConstructorId(2)),
                    span: 16..20
                },
                StringId::new("not") => Reservation {
                    id: NamespaceId::Function(FunctionId(3)),
                    span: 25..28
                }
            })
        );
    }

    #[test]
    fn test_error_if_inductive_and_function_have_same_name() {
        assert_eq!(
            str_to_ast("ind Bool {True, False} fn Bool() -> Bool {False}").unwrap_err(),
            DuplicateNameError {
                original: 4..8,
                duplicate: Ident::new(26, "Bool"),
            }
        );
    }

    fn str_to_definitions(code: &str) -> Result<ReservedNames, BoxedError> {
        let cst = crate::str_to_cst(code)?;
        ReservedNamesBuilder::new().visit(&cst)
    }
}
