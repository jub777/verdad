use super::argument::{cst_to_arguments, cst_to_type_ref};
use super::reservation::ReservedNames;
use crate::ast::FunctionSignature;
use crate::cst;
use crate::error::BoxedError;

pub fn cst_to_function_signature(
    globals: &ReservedNames,
    function: &cst::Function,
) -> Result<FunctionSignature, BoxedError> {
    let arguments = cst_to_arguments(globals, &function.arguments)?;
    Ok(FunctionSignature {
        name: function.name.clone(),
        return_class: cst_to_type_ref(globals, &function.return_class)?,
        arguments,
    })
}

#[cfg(test)]
mod test {
    use crate::abstractor::test::str_to_ast;
    use crate::ast::{
        Argument, Expression, FunctionDefinition, FunctionId, Inductive, InductiveId, Match,
        VariableId,
    };
    use crate::ast::{Definition, TypeRef};
    use crate::cst::Ident;

    #[test]
    fn test_function_return_type_resolved() {
        let ast = str_to_ast("ind Bool{False, True} fn false() -> Bool {False}").unwrap();
        assert_eq!(
            ast.definitions[FunctionId(3)].return_class.inductive_id,
            InductiveId(0),
        );
    }

    #[test]
    fn trivial_never_function() {
        let source = "ind Never{} fn drop(a: Never) -> Never {match a {}}";
        assert_eq!(
            str_to_ast(source).unwrap().definitions,
            vec![
                Definition::Inductive(Inductive {
                    name: Ident::new(4, "Never"),
                    arguments: vec![],
                    constructor_ids: vec![]
                }),
                Definition::Function(FunctionDefinition {
                    name: Ident::new(15, "drop"),
                    arguments: vec![Argument {
                        name: Ident::new(20, "a"),
                        class: TypeRef {
                            inductive_id: InductiveId(0),
                            ident: Ident::new(23, "Never"),
                        },
                    }],
                    return_class: TypeRef {
                        inductive_id: InductiveId(0),
                        ident: Ident::new(33, "Never"),
                    },
                    body: Expression::Match(Match {
                        argument: crate::ast::VariableRef {
                            ident: Ident::new(46, "a"),
                            variable_id: VariableId(0),
                            inductive_id: InductiveId(0),
                        },
                        arms: vec![],
                        span: (40..50)
                    })
                })
            ]
        );
    }
}
