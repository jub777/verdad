use crate::ast::{Call, ConstructorSignature, Inductive, InductiveId, SignatureNamespace};
use crate::error::{Diagnostic, Error, Label};
use crate::{cst, BoxedError, Span, StringId};

use super::reservation::ReservedNames;
use super::scope::StackScope;

pub fn cst_to_constructor_signature(
    globals: &ReservedNames,
    inductive_id: InductiveId,
    constructor: &cst::Constructor,
) -> Result<ConstructorSignature, BoxedError> {
    Ok(ConstructorSignature {
        name: constructor.name.clone(),
        arguments: super::argument::cst_to_arguments(globals, &constructor.arguments)?,
        inductive_id,
    })
}

pub fn cst_to_constructor_class(
    globals: &ReservedNames,
    signatures: &SignatureNamespace,
    constructor: &cst::Constructor,
) -> Result<Option<Call>, BoxedError> {
    let id = &globals.try_get(&constructor.name)?.id;
    let signature = &signatures[id.unwrap_constructor()];
    let parent = &signatures[signature.inductive_id];
    assert_constructor_declared_type_matches_parent(parent, signature, &constructor.class)?;
    let mut locals = StackScope::new_function(&signature.arguments)?;
    if let Some(call) = constructor.class.as_ref() {
        Ok(Some(super::expression::cst_to_call(
            globals,
            signatures,
            &mut locals,
            call,
        )?))
    } else {
        Ok(None)
    }
}

fn assert_constructor_declared_type_matches_parent(
    parent: &Inductive,
    constructor: &ConstructorSignature,
    declared: &Option<cst::Call>,
) -> Result<(), BoxedError> {
    match &declared {
        None if !parent.arguments.is_empty() => Err(Box::new(MissingConstructorType {
            inductive: parent.name.clone(),
            constructor: constructor.name.clone(),
        })),
        Some(inductive_call) if inductive_call.name.symbol != parent.name.symbol => Err(
            InvalidConstructorType::new_boxed(&parent.name, &inductive_call.name),
        ),
        _ => Ok(()),
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct InvalidConstructorType {
    expected: StringId,
    found: StringId,
    definition: Span,
    reference: Span,
}

impl InvalidConstructorType {
    fn new_boxed(inductive: &cst::Ident, constructor_class: &cst::Ident) -> BoxedError {
        Box::new(InvalidConstructorType {
            expected: inductive.symbol,
            found: constructor_class.symbol,
            definition: inductive.span.clone(),
            reference: constructor_class.span.clone(),
        })
    }
}

impl Error for InvalidConstructorType {
    fn span(&self) -> Span {
        self.reference.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "constructor must be the same type as it's parent inductive `{}`",
                self.expected
            ))
            .with_labels(vec![
                Label::primary(file_id, self.reference.clone()).with_message(format!(
                    "expected `{}` but found `{}`",
                    self.expected, self.found
                )),
                Label::primary(file_id, self.definition.clone()).with_message(format!(
                    "because it is defined inside of inductive `{}`",
                    self.expected
                )),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct MissingConstructorType {
    constructor: cst::Ident,
    inductive: cst::Ident,
}

impl Error for MissingConstructorType {
    fn span(&self) -> Span {
        self.constructor.span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "constructor `{}` is missing a type definition",
                self.constructor.symbol
            ))
            .with_labels(vec![
                Label::primary(file_id, self.constructor.span.clone()).with_message(format!(
                    "did not find expected type definition after constructor `{}`",
                    self.constructor.symbol
                )),
                Label::primary(file_id, self.inductive.span.clone()).with_message(format!(
                    "cannot omit it because parent inductive `{}` has one or more arguments",
                    self.inductive.symbol
                )),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{
        abstractor::{reservation::DuplicateNameError, test::str_to_ast},
        ast::{
            Argument, Call, ConstructorDefinition, ConstructorId, Definition, Expression,
            Inductive, InductiveId, NamespaceId, TypeRef, VariableId, VariableRef,
        },
        cst::Ident,
    };

    #[test]
    fn constructor_of_inductive_with_arguments() {
        assert_eq!(
            str_to_ast("ind Nil{} ind Eq(a: Nil, b: Nil){ Reflex(n: Nil): Eq(n, n) }")
                .unwrap()
                .definitions[ConstructorId(2)],
            ConstructorDefinition {
                name: Ident::new(34, "Reflex"),
                arguments: vec![Argument {
                    name: Ident::new(41, "n"),
                    class: TypeRef {
                        inductive_id: InductiveId(0),
                        ident: Ident::new(44, "Nil"),
                    }
                }],
                class: Some(Call {
                    ident: Ident::new(50, "Eq"),
                    namespace_id: NamespaceId::Inductive(InductiveId(1)),
                    arguments: vec![
                        Expression::VariableRef(VariableRef {
                            variable_id: VariableId(0),
                            ident: Ident::new(53, "n"),
                            inductive_id: InductiveId(0)
                        }),
                        Expression::VariableRef(VariableRef {
                            variable_id: VariableId(0),
                            ident: Ident::new(56, "n"),
                            inductive_id: InductiveId(0)
                        })
                    ],
                }),
            },
        );
    }

    #[test]
    fn constructor_must_have_matching_type_if_parent_inductive_has_arguments() {
        assert_eq!(
            str_to_ast("ind Never{} ind Foo(a: Never) { Bar }").unwrap_err(),
            MissingConstructorType {
                constructor: Ident::new(32, "Bar"),
                inductive: Ident::new(16, "Foo"),
            }
        );
    }

    #[test]
    fn constructor_types_must_match_inductive_parent() {
        assert_eq!(
            str_to_ast("ind Never{} ind Bool {False, True: Never}").unwrap_err(),
            InvalidConstructorType {
                found: StringId::new("Never"),
                expected: StringId::new("Bool"),
                definition: 16..20,
                reference: 35..40
            }
        );
    }

    #[test]
    fn trivial_never_function() {
        let source = "ind Int{Zero, NPlus(n: Int)}";
        assert_eq!(
            str_to_ast(source).unwrap().definitions,
            vec![
                Definition::Inductive(Inductive {
                    name: Ident::new(4, "Int"),
                    arguments: vec![],
                    constructor_ids: vec![ConstructorId(1), ConstructorId(2)]
                }),
                Definition::Constructor(ConstructorDefinition {
                    name: Ident::new(8, "Zero"),
                    class: None,
                    arguments: vec![]
                }),
                Definition::Constructor(ConstructorDefinition {
                    name: Ident::new(14, "NPlus"),
                    class: None,
                    arguments: vec![Argument {
                        name: Ident::new(20, "n"),
                        class: TypeRef {
                            inductive_id: InductiveId(0),
                            ident: Ident::new(23, "Int"),
                        }
                    }]
                }),
            ]
        );
    }

    #[test]
    fn duplicate_constructor_arguments_errors() {
        let source = "ind Foo{Bar(a: Foo, a: Foo)}";

        assert_eq!(
            str_to_ast(source).unwrap_err(),
            DuplicateNameError {
                original: 12..13,
                duplicate: Ident::new(20, "a")
            }
        );
    }
}
