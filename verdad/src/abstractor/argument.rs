use codespan_reporting::diagnostic::Label;

use crate::ast::{Argument, NamespaceId, TypeRef};
use crate::error::{BoxedError, Diagnostic};
use crate::{cst, Error, Span, StringId};

use super::reservation::ReservedNames;

pub fn cst_to_arguments(
    globals: &ReservedNames,
    arguments: &[cst::Argument],
) -> Result<Vec<Argument>, BoxedError> {
    arguments
        .iter()
        .map(|argument| cst_to_argument(globals, argument))
        .collect()
}

fn cst_to_argument(
    globals: &ReservedNames,
    argument: &cst::Argument,
) -> Result<Argument, BoxedError> {
    Ok(Argument {
        name: argument.name.clone(),
        class: cst_to_type_ref(globals, &argument.class)?,
    })
}

pub fn cst_to_type_ref(globals: &ReservedNames, ident: &cst::Ident) -> Result<TypeRef, BoxedError> {
    let id = &globals.try_get(ident)?.id;
    match id {
        NamespaceId::Inductive(inductive_id) => Ok(TypeRef {
            inductive_id: *inductive_id,
            ident: ident.clone(),
        }),
        NamespaceId::Constructor(_) => Err(Box::new(InductiveTypeNotFound {
            super_type: StringId::new("constructor"),
            found_type: ident.symbol,
            span: ident.span.clone(),
        })),
        NamespaceId::Function(_) => Err(Box::new(InductiveTypeNotFound {
            super_type: StringId::new("function"),
            found_type: ident.symbol,
            span: ident.span.clone(),
        })),
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct InductiveTypeNotFound {
    super_type: StringId,
    found_type: StringId,
    span: Span,
}

impl Error for InductiveTypeNotFound {
    fn span(&self) -> Span {
        self.span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message("argument types must be inductives".to_string())
            .with_labels(vec![Label::primary(file_id, self.span.clone())
                .with_message(format!(
                    "expected a inductive type but found `{}` of type {}",
                    self.found_type, self.super_type,
                ))])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::abstractor::reservation::DuplicateNameError;
    use crate::abstractor::test::str_to_ast;
    use crate::ast::{FunctionId, InductiveId};

    #[test]
    fn test_function_argument_types_resolved() {
        let source = "ind Never{} ind Bool{True,False} fn drop(a: Never,b: Bool) -> Bool {False}";

        let ast = str_to_ast(source).unwrap();
        assert_eq!(
            ast.definitions[FunctionId(4)]
                .arguments
                .iter()
                .map(|argument| argument.class.inductive_id)
                .collect::<Vec<_>>(),
            vec![InductiveId(0), InductiveId(1)]
        );
    }

    #[test]
    fn duplicate_function_arguments_errors() {
        let source = "ind Bool{True,False} fn drop(a: Bool,a: Bool) -> Bool {False}";

        assert_eq!(
            str_to_ast(source).unwrap_err(),
            DuplicateNameError {
                original: 29..30,
                duplicate: cst::Ident::new(37, "a")
            }
        );
    }

    #[test]
    fn duplicated_argument_names_across_seperate_functions() {
        let source = "ind Never{} fn alpha(a: Never) -> Never {match a {}} fn beta(a: Never) -> Never {match a {}}";

        str_to_ast(source).unwrap();
    }

    #[test]
    fn arguments_not_inductive_types() {
        let source = "ind Int(a: Zero){Zero}";

        assert_eq!(
            str_to_ast(source).unwrap_err(),
            InductiveTypeNotFound {
                super_type: StringId::new("constructor"),
                found_type: StringId::new("Zero"),
                span: 11..15,
            }
        );
    }
}
