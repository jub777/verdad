mod argument;
mod constructor;
mod expression;
mod function;
mod inductive;
mod namespace;
mod reservation;
mod scope;
mod signature;

use crate::ast::Ast;
use crate::cst;
use crate::cst::Visitor;
use crate::error::BoxedError;

pub fn cst_to_ast(syntax: &cst::Cst) -> Result<Ast, BoxedError> {
    let reservations = reservation::ReservedNamesBuilder::new().visit(syntax)?;
    let signatures = signature::SignatureBuilder::new(&reservations).visit(syntax)?;
    let definitions = namespace::NamespaceBuilder::new(&reservations, signatures).visit(syntax)?;
    Ok(Ast { definitions })
}

#[cfg(test)]
pub mod test {
    use super::*;
    use crate::{ast::DefinitionNamespace, str_to_cst};

    pub fn str_to_ast(source: &str) -> Result<Ast, BoxedError> {
        let cst = str_to_cst(source).unwrap();
        cst_to_ast(&cst)
    }

    #[test]
    fn test_empty_cst_to_empty_ast() {
        assert_eq!(
            str_to_ast("").unwrap(),
            Ast {
                definitions: DefinitionNamespace::default(),
            }
        );
    }
}
