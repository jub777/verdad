use super::reservation::ReservedNames;
use super::scope::StackScope;
use crate::ast::{
    Call, ConstructorDefinition, Definition, DefinitionNamespace, Expression, FunctionDefinition,
    Signature, SignatureNamespace,
};
use crate::cst;
use crate::error::BoxedError;

pub struct NamespaceBuilder<'a> {
    reservations: &'a ReservedNames,
    signatures: SignatureNamespace,
    bodies: Vec<Expression>,
    constructor_classes: Vec<Option<Call>>,
}

impl<'a> NamespaceBuilder<'a> {
    pub fn new(reservations: &'a ReservedNames, signatures: SignatureNamespace) -> Self {
        Self {
            reservations,
            signatures,
            bodies: Vec::new(),
            constructor_classes: Vec::new(),
        }
    }
}

impl<'a> cst::Visitor for NamespaceBuilder<'a> {
    type Output = DefinitionNamespace;
    type Error = BoxedError;

    fn visit_function(&mut self, function: &cst::Function) -> Result<(), Self::Error> {
        let id = &self.reservations.try_get(&function.name)?.id;
        let signature = &self.signatures[id.unwrap_function()];
        self.bodies.push(super::expression::cst_to_expression(
            self.reservations,
            &self.signatures,
            &mut StackScope::new_function(&signature.arguments)?,
            &function.body,
        )?);
        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &cst::Constructor) -> Result<(), Self::Error> {
        self.constructor_classes
            .push(super::constructor::cst_to_constructor_class(
                self.reservations,
                &self.signatures,
                constructor,
            )?);
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        let mut bodies = self.bodies.into_iter();
        let mut inductive_calls = self.constructor_classes.into_iter();
        Ok(self.signatures.map(|signature| match signature {
            Signature::Inductive(inductive) => Definition::Inductive(inductive),
            Signature::Constructor(constructor) => Definition::Constructor(ConstructorDefinition {
                name: constructor.name,
                arguments: constructor.arguments,
                class: inductive_calls.next().unwrap(),
            }),
            Signature::Function(function) => Definition::Function(FunctionDefinition {
                name: function.name,
                arguments: function.arguments,
                return_class: function.return_class,
                body: bodies.next().unwrap(),
            }),
        }))
    }
}

#[cfg(test)]
mod test {
    use crate::{
        abstractor::test::str_to_ast,
        ast::{
            Call, ConstructorDefinition, ConstructorId, Definition, Expression, FunctionDefinition,
            Inductive, InductiveId, NamespaceId, TypeRef,
        },
        cst::Ident,
    };

    #[test]
    fn test_single_inductive_definition_to_ast() {
        assert_eq!(
            str_to_ast("ind nat {}").unwrap().definitions,
            vec![Definition::Inductive(Inductive {
                name: Ident::new(4, "nat"),
                arguments: vec![],
                constructor_ids: vec![],
            })]
        );
    }

    #[test]
    fn test_inductive_with_constructors_added_to_namespace() {
        let source = "ind Bool {True, False} fn false() -> Bool { False }";

        assert_eq!(
            str_to_ast(source).unwrap().definitions,
            vec![
                Definition::Inductive(Inductive {
                    name: Ident::new(4, "Bool"),
                    arguments: vec![],
                    constructor_ids: vec![ConstructorId(1), ConstructorId(2)]
                }),
                Definition::Constructor(ConstructorDefinition {
                    name: Ident::new(10, "True"),
                    class: None,
                    arguments: vec![]
                }),
                Definition::Constructor(ConstructorDefinition {
                    name: Ident::new(16, "False"),
                    class: None,
                    arguments: vec![]
                }),
                Definition::Function(FunctionDefinition {
                    name: Ident::new(26, "false"),
                    arguments: vec![],
                    return_class: TypeRef {
                        inductive_id: InductiveId(0),
                        ident: Ident::new(37, "Bool"),
                    },
                    body: Expression::ConstructorCall(Call {
                        ident: Ident::new(44, "False"),
                        namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                        arguments: vec![]
                    })
                })
            ]
        );
    }
}
