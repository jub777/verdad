use crate::{ast::Visitor, error::BoxedError};

#[derive(Default)]
pub struct VariableTypeVisitor {}

impl<'a> Visitor<'a> for VariableTypeVisitor {
    type Output = VariableTypes;
    type Error = BoxedError;

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(VariableTypes::default())
    }
}

#[derive(Debug, Default, Eq, PartialEq)]
pub struct VariableTypes {}

#[cfg(test)]
mod test {
    use super::*;
    use crate::abstractor::test::str_to_ast;

    #[test]
    fn empty_expression_gives_empty_table() {
        assert_eq!(get_variable_types("").unwrap(), VariableTypes::default())
    }

    fn get_variable_types(source: &str) -> Result<VariableTypes, BoxedError> {
        VariableTypeVisitor::default().visit(&str_to_ast(source)?)
    }
}
