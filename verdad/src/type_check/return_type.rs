use crate::{
    ast::{ConstructorCall, DefinitionNamespace, Expression, InductiveId, Match, VariableRef},
    error::{Diagnostic, Error, Label},
    BoxedError, Span, StringId,
};

pub fn assert_type_equal(
    namespace: &DefinitionNamespace,
    expected: &ReturnType,
    found: &ReturnType,
) -> Result<(), BoxedError> {
    if expected.is_equal(found, namespace) {
        return Ok(());
    }
    Err(Box::new(TypeMismatchError {
        expected_name: inductive_id_name(namespace, expected.inductive_id),
        expected_span: expected.span.clone(),
        found_name: inductive_id_name(namespace, found.inductive_id),
        found_span: found.span.clone(),
    }))
}

pub struct ReturnType {
    pub inductive_id: Option<InductiveId>,
    pub span: Span,
}

impl ReturnType {
    fn is_equal(&self, other: &ReturnType, namespace: &DefinitionNamespace) -> bool {
        match (self.inductive_id, other.inductive_id) {
            (Some(expected), Some(found)) => expected == found,
            (Some(expected), None) => namespace[expected].constructor_ids.is_empty(),
            (None, Some(found)) => namespace[found].constructor_ids.is_empty(),
            (None, None) => true,
        }
    }
}

impl Expression {
    pub fn return_type(&self, namespace: &DefinitionNamespace) -> Result<ReturnType, BoxedError> {
        match self {
            Self::Match(match_expression) => match_expression.return_type(namespace),
            Self::ConstructorCall(call) => call.return_type(namespace),
            Self::VariableRef(variable_ref) => variable_ref.return_type(),
        }
    }
}

impl Match {
    fn return_type(&self, namespace: &DefinitionNamespace) -> Result<ReturnType, BoxedError> {
        let mut arm_return_types = self
            .arms
            .iter()
            .map(|arm| arm.expression.return_type(namespace));
        let expected_return = match arm_return_types.next() {
            Some(return_type) => return_type?,
            None => ReturnType {
                inductive_id: None,
                span: self.span.clone(),
            },
        };
        for arm_return in arm_return_types {
            assert_type_equal(namespace, &expected_return, &arm_return?)?;
        }
        Ok(expected_return)
    }
}

impl ConstructorCall {
    fn return_type(&self, namespace: &DefinitionNamespace) -> Result<ReturnType, BoxedError> {
        Ok(ReturnType {
            inductive_id: Some(namespace[self.constructor_id].class.inductive_id),
            span: self.ident.span.clone(),
        })
    }
}

impl VariableRef {
    fn return_type(&self) -> Result<ReturnType, BoxedError> {
        Ok(ReturnType {
            inductive_id: Some(self.inductive_id),
            span: self.ident.span.clone(),
        })
    }
}

fn inductive_id_name(
    namespace: &DefinitionNamespace,
    inductive_id: Option<InductiveId>,
) -> Option<StringId> {
    inductive_id.map(|id| namespace[id].name.symbol)
}

#[derive(Debug, PartialEq, Eq)]
pub struct TypeMismatchError {
    pub expected_name: Option<StringId>,
    pub expected_span: Span,
    pub found_name: Option<StringId>,
    pub found_span: Span,
}

impl Error for TypeMismatchError {
    fn span(&self) -> Span {
        self.found_span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message("mismatched types")
            .with_labels(vec![
                Label::primary(file_id, self.expected_span.clone()).with_message(format!(
                    "expected type `{}` because of this",
                    self.expected_name.map(|id| id.as_str()).unwrap_or("(none)")
                )),
                Label::primary(file_id, self.found_span.clone()).with_message(format!(
                    "expected type `{}`, found type `{}`",
                    &self.expected_name.map(|id| id.as_str()).unwrap_or("(none)"),
                    &self.found_name.map(|id| id.as_str()).unwrap_or("(none)")
                )),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}
