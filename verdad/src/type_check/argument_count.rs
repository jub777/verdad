use crate::{
    ast::{Argument, ConstructorCall, DefinitionNamespace, Expression, Visitor},
    cst::Ident,
    error::{BoxedError, Diagnostic, Error, Label},
    Span,
};

pub struct ArgumentCountChecker<'ast> {
    namespace: &'ast DefinitionNamespace,
}

impl<'ast> ArgumentCountChecker<'ast> {
    pub fn new(namespace: &'ast DefinitionNamespace) -> Self {
        Self { namespace }
    }

    fn check_argument_count(
        definition: &Ident,
        expected: &[Argument],
        call: &Ident,
        found: &[Expression],
    ) -> Result<(), BoxedError> {
        if expected.len() == found.len() {
            Ok(())
        } else {
            Err(Box::new(IncorrectArgumentCount {
                expected: expected.len(),
                found: found.len(),
                definition: definition.span.clone(),
                call: call.span.clone(),
            }))
        }
    }
}

impl<'a> Visitor<'a> for ArgumentCountChecker<'a> {
    type Output = ();
    type Error = BoxedError;

    fn visit_constructor(
        &mut self,
        constructor: &'a crate::ast::ConstructorDefinition,
    ) -> Result<(), Self::Error> {
        let inductive = &self.namespace[constructor.class.inductive_id];
        Self::check_argument_count(
            &inductive.name,
            &inductive.arguments,
            constructor.class.name.as_ref().unwrap_or(&constructor.name),
            &constructor.class.arguments,
        )?;
        crate::ast::walk_constructor(self, constructor)?;
        Ok(())
    }

    fn visit_call(&mut self, call: &'a ConstructorCall) -> Result<(), Self::Error> {
        let constructor = &self.namespace[call.object_id.unwrap_as_constructor()];
        Self::check_argument_count(
            &constructor.name,
            &constructor.arguments,
            &call.ident,
            &call.arguments,
        )?;
        crate::ast::walk_call(self, call)?;
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct IncorrectArgumentCount {
    expected: usize,
    found: usize,
    definition: Span,
    call: Span,
}

impl Error for IncorrectArgumentCount {
    fn span(&self) -> Span {
        self.call.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "this takes {} arguments, but {} arguments were supplied",
                self.found, self.expected
            ))
            .with_labels(vec![
                Label::primary(file_id, self.call.clone())
                    .with_message(format!("found {} arguments here", self.found)),
                Label::primary(file_id, self.definition.clone()).with_message(format!(
                    "expected {} arguments because of definition here",
                    self.expected
                )),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::check;

    #[test]
    fn argument_count_checked_recursively_in_inductive_calls() {
        assert_eq!(
            check("ind Int { Zero, Inc(n: Int) } ind Foo(a: Int) { Bar: Foo(Inc) }").unwrap_err(),
            IncorrectArgumentCount {
                expected: 1,
                found: 0,
                definition: 16..19,
                call: 57..60
            }
        );
    }

    #[test]
    fn inductive_call_missing_arugment_raises_error() {
        assert_eq!(
            check("ind Nil{} ind Foo{Bar(a: Nil): Foo(a)}").unwrap_err(),
            IncorrectArgumentCount {
                expected: 0,
                found: 1,
                definition: 14..17,
                call: 31..34
            }
        );
    }

    #[test]
    fn call_missing_arugment_raises_error() {
        assert_eq!(
            check("ind Int{Zero, Inc(n: Int)} fn foo() -> Int {Inc}").unwrap_err(),
            IncorrectArgumentCount {
                expected: 1,
                found: 0,
                definition: 14..17,
                call: 44..47
            }
        );
    }

    #[test]
    fn nested_call_missing_argument_raises_error() {
        assert_eq!(
            check("ind Int{Zero, Inc(n: Int)} fn foo() -> Int {Inc(Inc)}").unwrap_err(),
            IncorrectArgumentCount {
                expected: 1,
                found: 0,
                definition: 14..17,
                call: 48..51
            }
        );
    }
}
