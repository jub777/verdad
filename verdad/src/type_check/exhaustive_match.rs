use itertools::Itertools;

use super::return_type::TypeMismatchError;
use crate::ast::{
    BindConstructor, ConstructorId, DefinitionNamespace, Match, VariableRef, Visitor,
};
use crate::cst::Ident;
use crate::error::{BoxedError, Diagnostic, Error, Label};
use crate::Span;

pub struct ExhaustiveMatchChecker<'ast> {
    namespace: &'ast DefinitionNamespace,
}

impl<'ast> ExhaustiveMatchChecker<'ast> {
    pub fn new(namespace: &'ast DefinitionNamespace) -> Self {
        Self { namespace }
    }

    fn check_patterns(&self, match_definition: &'ast Match) -> Result<(), BoxedError> {
        let expected_patterns = self.expected_patterns(match_definition);
        let existing_patterns = self.existing_patterns(match_definition);
        self.assert_all_patterns_match_argument_type(
            &existing_patterns,
            &match_definition.argument,
        )?;
        self.assert_all_patterns_covered(
            expected_patterns,
            &existing_patterns,
            &match_definition.argument,
        )?;
        self.assert_no_duplicate_patterns(&existing_patterns)?;

        Ok(())
    }

    fn expected_patterns(&self, match_definition: &'ast Match) -> &'ast [ConstructorId] {
        &self.namespace[match_definition.argument.inductive_id].constructor_ids
    }

    fn existing_patterns(&self, match_definition: &'ast Match) -> Vec<&BindConstructor> {
        match_definition
            .arms
            .iter()
            .map(|arm| &arm.pattern)
            .collect()
    }

    fn assert_all_patterns_match_argument_type(
        &self,
        existing_patterns: &[&BindConstructor],
        argument: &VariableRef,
    ) -> Result<(), BoxedError> {
        if let Some(pattern) = existing_patterns
            .iter()
            .find(|pattern| self.pattern_doesnt_match_argument_type(argument, pattern))
        {
            let argument_type = &self.namespace[argument.inductive_id];
            let pattern_constructor = &self.namespace[pattern.constructor_id];
            let pattern_type = &self.namespace[pattern_constructor.class.inductive_id];
            Err(Box::new(TypeMismatchError {
                expected_name: Some(argument_type.name.symbol),
                expected_span: argument.ident.span.clone(),
                found_name: Some(pattern_type.name.symbol),
                found_span: pattern.ident.span.clone(),
            }))
        } else {
            Ok(())
        }
    }

    fn pattern_doesnt_match_argument_type(
        &self,
        argument: &VariableRef,
        pattern: &BindConstructor,
    ) -> bool {
        !self.namespace[argument.inductive_id]
            .constructor_ids
            .contains(&pattern.constructor_id)
    }

    fn assert_all_patterns_covered(
        &self,
        expected_patterns: &[ConstructorId],
        existing_patterns: &[&BindConstructor],
        argument: &'ast VariableRef,
    ) -> Result<(), BoxedError> {
        let missing = Self::missing_patterns(expected_patterns, existing_patterns);
        if missing.is_empty() {
            return Ok(());
        }
        Err(Box::new(PatternNotCovered {
            patterns: missing
                .iter()
                .map(|pattern| self.namespace[*pattern].name.clone())
                .collect(),
            argument_span: argument.ident.span.clone(),
            argument_type: self.namespace[argument.inductive_id].name.clone(),
        }))
    }

    fn assert_no_duplicate_patterns(
        &self,
        existing_patterns: &[&BindConstructor],
    ) -> Result<(), BoxedError> {
        let duplicated = existing_patterns
            .iter()
            .duplicates_by(|pattern| &pattern.constructor_id)
            .next();
        match duplicated {
            None => Ok(()),
            Some(duplicate) => {
                let first = existing_patterns
                    .iter()
                    .find(|pattern| pattern.constructor_id == duplicate.constructor_id)
                    .unwrap();
                Err(Box::new(DuplicatePatternError {
                    first: first.ident.clone(),
                    duplicate: duplicate.ident.span.clone(),
                }))
            }
        }
    }

    fn missing_patterns(
        expected_patterns: &[ConstructorId],
        existing_patterns: &[&BindConstructor],
    ) -> Vec<ConstructorId> {
        expected_patterns
            .iter()
            .filter(|expected| {
                !existing_patterns
                    .iter()
                    .any(|existing| expected == &&existing.constructor_id)
            })
            .cloned()
            .collect()
    }
}

impl<'ast> Visitor<'ast> for ExhaustiveMatchChecker<'ast> {
    type Output = ();
    type Error = BoxedError;

    fn visit_match(&mut self, match_definition: &'ast Match) -> Result<(), Self::Error> {
        self.check_patterns(match_definition)?;
        crate::ast::walk_match(self, match_definition)?;
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct PatternNotCovered {
    patterns: Vec<Ident>,
    argument_span: Span,
    argument_type: Ident,
}

impl Error for PatternNotCovered {
    fn span(&self) -> Span {
        self.argument_span.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        let patterns_list = format_english_list(&self.patterns);
        Diagnostic::error()
            .with_message(format!(
                "non-exhaustive patterns: {} not covered",
                patterns_list
            ))
            .with_labels(vec![
                Label::primary(file_id, self.argument_span.clone())
                    .with_message(format!("patterns {} not covered", patterns_list)),
                Label::secondary(file_id, self.argument_type.span.clone()).with_message(format!(
                    "note: the matched value is of type `{}`",
                    self.argument_type.symbol
                )),
            ])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

fn format_english_list(idents: &[Ident]) -> String {
    match idents {
        [] => panic!("Not enough strings to combine into english list"),
        [singular] => format!("`{}`", singular.symbol),
        [first, second] => format!("`{}` and `{}`", first.symbol, second.symbol),
        [prefixes @ .., suffix] => format!(
            "`{}` and `{}`",
            itertools::Itertools::intersperse(
                prefixes.iter().map(|prefix| prefix.symbol.as_str()),
                "`, `"
            )
            .collect::<String>(),
            suffix.symbol
        ),
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct DuplicatePatternError {
    first: Ident,
    duplicate: Span,
}

impl Error for DuplicatePatternError {
    fn span(&self) -> Span {
        self.duplicate.clone()
    }

    fn diagnostic(&self, file_id: usize) -> crate::error::Diagnostic {
        Diagnostic::error()
            .with_message(format!(
                "the pattern `{}` is defined multiple times",
                self.first.symbol
            ))
            .with_labels(vec![
                Label::primary(file_id, self.first.span.clone()).with_message(format!(
                    "previous definition of `{}` here",
                    self.first.symbol
                )),
                Label::primary(file_id, self.duplicate.clone())
                    .with_message(format!("redefinition of `{}` here", self.first.symbol)),
            ])
            .with_notes(vec![format!(
                "`{}` must be defined only in one arm of this match expression",
                self.first.symbol
            )])
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{check, StringId};

    #[test]
    fn nested_exhaustive_match() {
        check("ind Never{} ind Foo{Bar(a: Never)} fn foo(a: Foo) -> Never {match a {Bar(b) -> match b{}}}").unwrap();
    }

    #[test]
    fn missing_two_out_of_two_match_arms_gives_pattern_not_covered_error() {
        assert_eq!(
            check("ind Bool{True, False} fn not(a: Bool) -> Bool {match a {}}").unwrap_err(),
            PatternNotCovered {
                patterns: vec![Ident::new(9, "True"), Ident::new(15, "False")],
                argument_span: (53..54),
                argument_type: Ident::new(4, "Bool"),
            }
        );
    }

    #[test]
    fn duplicate_patterns_error() {
        assert_eq!(
            check("ind Bool{True, False} fn not(a: Bool) -> Bool {match a {True->False,False->True,False->True}}").unwrap_err(), 
            DuplicatePatternError {
                first: Ident::new(68, "False"),
                duplicate: (80..85),
            }
        );
    }

    #[test]
    fn format_english_list_from_slice_of_strs() {
        let list = ["A", "B", "C", "D"]
            .into_iter()
            .map(|string| Ident::new(0, string))
            .collect::<Vec<Ident>>();
        assert_eq!(format_english_list(&list[0..1]), "`A`");
        assert_eq!(format_english_list(&list[0..2]), "`A` and `B`");
        assert_eq!(format_english_list(&list[0..3]), "`A`, `B` and `C`");
        assert_eq!(format_english_list(&list), "`A`, `B`, `C` and `D`");
    }

    #[test]
    fn variants_from_wrong_type_in_match_returns_type_mismatch_error() {
        assert_eq!(
            check("ind Bool{True, False} ind Never{} fn not(a: Never) -> Bool {match a {True->False,False->True}}").unwrap_err(), 
            TypeMismatchError {
                expected_name: Some(StringId::new("Never")),
                expected_span: 66..67,
                found_name: Some(StringId::new("Bool")),
                found_span: 69..73,
            }
        );
    }
}
