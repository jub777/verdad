mod argument_count;
mod exhaustive_match;
mod expression_type;
mod return_type;
mod variable_type;

use crate::ast::{Ast, Visitor};
use crate::error::BoxedError;

pub fn type_check(ast: &Ast) -> Result<(), BoxedError> {
    argument_count::ArgumentCountChecker::new(&ast.definitions).visit(ast)?;
    exhaustive_match::ExhaustiveMatchChecker::new(&ast.definitions).visit(ast)?;
    expression_type::ExpressionTypeChecker::new(&ast.definitions).visit(ast)?;
    Ok(())
}
