use super::return_type::{assert_type_equal, ReturnType};
use crate::ast::{Argument, DefinitionNamespace, Expression, FunctionDefinition, Visitor};
use crate::error::BoxedError;

pub struct ExpressionTypeChecker<'a> {
    namespace: &'a DefinitionNamespace,
}

impl<'a> ExpressionTypeChecker<'a> {
    pub fn new(namespace: &'a DefinitionNamespace) -> Self {
        Self { namespace }
    }

    fn check_argument_types(
        &self,
        arguments: &[Argument],
        expressions: &[Expression],
    ) -> Result<(), BoxedError> {
        for (expected, found) in arguments.iter().zip(expressions.iter()) {
            assert_type_equal(
                self.namespace,
                &ReturnType {
                    inductive_id: Some(expected.class.inductive_id),
                    span: expected.class.ident.span.clone(),
                },
                &found.return_type(self.namespace)?,
            )?;
        }
        Ok(())
    }
}

impl<'a> Visitor<'a> for ExpressionTypeChecker<'a> {
    type Output = ();
    type Error = BoxedError;

    fn visit_inductive_call(
        &mut self,
        call: &'a crate::ast::InductiveCall,
    ) -> Result<(), Self::Error> {
        let inductive = &self.namespace[call.inductive_id];
        self.check_argument_types(&inductive.arguments, &call.arguments)?;
        crate::ast::walk_inductive_call(self, call)?;
        Ok(())
    }

    fn visit_call(&mut self, call: &'a crate::ast::ConstructorCall) -> Result<(), Self::Error> {
        let constructor = &self.namespace[call.object_id.unwrap_as_constructor()];
        self.check_argument_types(&constructor.arguments, &call.arguments)?;
        crate::ast::walk_call(self, call)?;
        Ok(())
    }

    fn visit_function(&mut self, function: &'a FunctionDefinition) -> Result<(), Self::Error> {
        assert_type_equal(
            self.namespace,
            &ReturnType {
                inductive_id: Some(function.return_class.inductive_id),
                span: function.return_class.ident.span.clone(),
            },
            &function.body.return_type(self.namespace)?,
        )?;
        crate::ast::walk_function(self, function)?;
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::{check, type_check::return_type::TypeMismatchError, StringId};

    #[test]
    fn argument_type_checked_recursively_in_inductive_calls() {
        assert_eq!(
            check("ind A { B(c: C) } ind C { D } ind X { Y } ind Foo(a: A) { Bar: Foo(B(Y)) }")
                .unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("C")),
                expected_span: 13..14,
                found_name: Some(StringId::new("X")),
                found_span: 69..70,
            }
        );
    }

    #[test]
    fn incorrect_inductive_argument_type_causes_error() {
        assert_eq!(
            check("ind A { B } ind X { Y } ind Foo(a: A) { Bar(x: X): Foo(x) }").unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("A")),
                expected_span: 35..36,
                found_name: Some(StringId::new("X")),
                found_span: 55..56,
            }
        );
    }

    #[test]
    fn incorrect_constructor_argument_type_causes_error() {
        assert_eq!(
            check("ind Unit{One} ind Int{Zero, Inc(a: Int)} fn foo() -> Int { Inc(One) }")
                .unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("Int")),
                expected_span: 35..38,
                found_name: Some(StringId::new("Unit")),
                found_span: 63..66,
            }
        );
    }

    #[test]
    fn correct_return_types() {
        check("ind Bool {True, False} fn false() -> Bool {False}").unwrap();
        check(
            "ind Bool {True, False} fn not(a: Bool) -> Bool {match a { False->True, True->False }}",
        )
        .unwrap();
        check("ind Never {} fn never(a: Never) -> Never {match a {}}").unwrap();
    }

    #[test]
    fn trivial_incorrect_call_expression_type_gives_type_mismatch_error() {
        assert_eq!(
            check("ind Never{} ind Bool{True, False} fn false() -> Never {False}").unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("Never")),
                expected_span: 48..53,
                found_name: Some(StringId::new("Bool")),
                found_span: 55..60,
            }
        );
    }

    #[test]
    fn match_with_wrong_arm_type_gives_type_mismatch_error() {
        assert_eq!(
            check(
                "ind Letter{A,B,C} ind Bool{True, False} fn not(a: Bool) -> Bool {match a {False->B, True->False}}").unwrap_err(), 
            TypeMismatchError {
                expected_name: Some(StringId::new("Letter")),
                expected_span: 81..82,
                found_name: Some(StringId::new("Bool")),
                found_span: 90..95,
            }
        );
        assert_eq!(
            check("ind A{B} ind X{Y} fn foo(a: A) -> X { match a { B -> a } }").unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("X")),
                expected_span: 34..35,
                found_name: Some(StringId::new("A")),
                found_span: 53..54,
            }
        );
    }

    #[test]
    fn nested_match_with_wrong_return_type_gives_type_mismatch_error() {
        assert_eq!(
            check(
                "ind A{B} ind X{Y(a: A)} fn foo(x: X) -> X { match x { Y(a) -> match a { B -> B } } }",
            ).unwrap_err(),
            TypeMismatchError {
                expected_name: Some(StringId::new("X")),
                expected_span: 40..41,
                found_name: Some(StringId::new("A")),
                found_span: 77..78,
            }
        );
    }
}
