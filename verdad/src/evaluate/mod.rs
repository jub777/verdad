use crate::ast::{Expression, NamespaceId};

#[derive(Debug, PartialEq, Eq)]
struct Instance {
    namespace_id: NamespaceId,
    arguments: Vec<Instance>,
}

#[allow(dead_code)]
fn simplify(expression: &Expression) -> Instance {
    match expression {
        Expression::ConstructorCall(constructor) => Instance {
            namespace_id: constructor.namespace_id.clone(),
            arguments: constructor.arguments.iter().map(simplify).collect(),
        },
        _ => todo!(),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{
        abstractor::test::str_to_ast,
        ast::{ConstructorId, FunctionId},
    };

    #[test]
    fn simplify_simple_bool_call_false() {
        let ast = str_to_ast("ind Bool {False, True} fn foo() -> Bool { False }").unwrap();
        let expression = &ast.definitions[FunctionId(3)].body;
        assert_eq!(
            simplify(expression),
            Instance {
                namespace_id: NamespaceId::Constructor(ConstructorId(1)),
                arguments: vec![]
            }
        );
    }

    #[test]
    fn simplify_simple_bool_call_true() {
        let ast = str_to_ast("ind Bool {False, True} fn foo() -> Bool { True }").unwrap();
        let expression = &ast.definitions[FunctionId(3)].body;
        assert_eq!(
            simplify(expression),
            Instance {
                namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                arguments: vec![]
            }
        );
    }

    #[test]
    fn simplify_constructor_with_argument() {
        let ast = str_to_ast("ind Int {Zero, Inc(n: Int)} fn foo() -> Int { Inc(Zero) }").unwrap();
        let expression = &ast.definitions[FunctionId(3)].body;
        assert_eq!(
            simplify(expression),
            Instance {
                namespace_id: NamespaceId::Constructor(ConstructorId(2)),
                arguments: vec![Instance {
                    namespace_id: NamespaceId::Constructor(ConstructorId(1)),
                    arguments: vec![]
                }]
            }
        );
    }
}
