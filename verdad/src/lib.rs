//!
//! The structure of the folders closely follows the flow of source through the compiler:
//!
//!
//! Source -|Lexer|-> Thin Tokens -|Interner|-> Fat Tokens -|Parser|-> CST -|Abstractor|-> AST -> |Type Check|
//!                                                                     |
//!                                                                     +---|Lint|-------> Warnings
//!                                                                     |
//!                                                                     +---|Format|-----> Source
//!
//!
//! 1)  Source:      This is the source file of a module, typically a `&str` representing the contents of a file.
//! 2)  Lexer:       [`crate::lexer`] contains all the code to parse the source into [`token::ThinToken`]s.
//! 3)  Thin Tokens: These are defined in [`crate::token`].
//! 4)  Interner:    Repeating token strings (such as Inductive names) are internered, turning [`token::ThinToken`] into [`token::FatToken`].
//! 5)  Fat Tokens:  These are also defined in [`crate::token`], and are essentially Thin Tokens with maybe a StringId attached.
//! 6)  Parser:      The parser handles the syntax of the code, transforming [`token::FatToken`]s into the [`cst::Cst`].
//! 7)  CST:         The CST (Concrete Syntax Tree) is defined in [`crate::cst`], and is structure describing the syntax of a module.
//! 7a) Lint:        The code in [`crate::lint`] walks the [`cst::Cst`] and generates warnings about bad code patterns that aren't hard errors.
//! 8)  Abstractor:  The abstractor code in [`crate::abstractor`] turns the CST into a more abstract [`ast::Ast`] representation.
//!                  It's main job is to resolve all ident names into object or variable indices.
//!                  It also relsolves abiguitites that aren't possible in CST (such as if an ident is constructor or functrion name).
//! 9)  AST:         The AST is defined in [`crate::ast`] and represents an abstract compiler-friendly representation of the module.
//! 10) Type Check:  The AST has types checked to make sure that variables are the correct type for there location (e.g. matching return types).
//!
mod abstractor;
mod ast;
mod cst;
mod error;
mod evaluate;
mod formatter;
mod interner;
mod lexer;
mod lint;
mod parser;
mod token;
//mod type_check;

pub use error::{error_diagnostic_to_string, BoxedError, Error};
pub use formatter::cst_to_str;
pub use interner::StringId;
pub use lexer::str_to_tokens;
pub use token::{Span, Symbol, ThinTokenKind};

pub fn check(source: &str) -> Result<Vec<BoxedError>, BoxedError> {
    let cst = str_to_cst(source)?;
    let ast = abstractor::cst_to_ast(&cst)?;
    //type_check::type_check(&ast)?;
    Ok(lint::all_warnings(&cst))
}

pub fn str_to_cst(source: &str) -> Result<cst::Cst, BoxedError> {
    let thin_tokens = lexer::str_to_tokens(source).filter(token::ThinToken::is_not_whitespace);
    let fat_tokens = crate::interner::thin_to_fat_tokens(source, thin_tokens).collect::<Vec<_>>();
    let cst = parser::tokens_to_cst(fat_tokens)?;
    Ok(cst)
}

pub fn format_source(source: &str) -> Result<String, BoxedError> {
    let cst = str_to_cst(source)?;
    Ok(cst_to_str(&cst))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_empty_program_has_no_errors() {
        assert!(check("").is_ok());
    }
}
