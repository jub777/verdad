use crate::token::Span;
use codespan_reporting::{files::SimpleFiles, term::termcolor::NoColor};
use std::any::Any;

pub use codespan_reporting::diagnostic::Label;
pub type Diagnostic = codespan_reporting::diagnostic::Diagnostic<usize>;
pub type BoxedError = Box<dyn Error + 'static>;

pub trait Error: std::fmt::Debug + Any {
    fn span(&self) -> Span;
    fn diagnostic(&self, file_id: usize) -> Diagnostic;
    fn as_any(&self) -> &dyn Any;
}

impl<T: Error> From<T> for BoxedError {
    fn from(error: T) -> Self {
        Box::new(error)
    }
}

impl<T: Error + PartialEq> PartialEq<T> for BoxedError {
    fn eq(&self, other: &T) -> bool {
        self.as_any()
            .downcast_ref::<T>()
            .map(|error| error == other)
            .unwrap_or(false)
    }
}

pub trait DowncastErrVec {
    fn downcast_err_vec<T: Error>(&self) -> Vec<&T>;
}

impl DowncastErrVec for Vec<BoxedError> {
    fn downcast_err_vec<T: Error>(&self) -> Vec<&T> {
        self.iter()
            .map(|error| error.as_any().downcast_ref::<T>().unwrap())
            .collect()
    }
}

pub fn error_diagnostic_to_string(path: &str, source: &str, error: &dyn Error) -> String {
    let mut files: SimpleFiles<&str, &str> = SimpleFiles::new();
    let file_id = files.add(path, source);
    let mut writer = NoColor::new(Vec::new());
    let config = codespan_reporting::term::Config::default();
    let diagnostic = error.diagnostic(file_id);
    codespan_reporting::term::emit(&mut writer, &config, &files, &diagnostic).unwrap();
    String::from_utf8(writer.into_inner()).unwrap()
}

#[cfg(test)]
pub mod test {

    use super::*;

    #[test]
    fn test_boxed_error_only_equal_if_same_type_and_location() {
        assert_eq!(
            Box::<dyn Error>::from(Foo { location: 0 }),
            Foo { location: 0 }
        );
        assert_ne!(
            Box::<dyn Error>::from(Foo { location: 0 }),
            Foo { location: 1 }
        );
        assert_ne!(Box::<dyn Error>::from(Foo { location: 0 }), Bar {});
    }

    #[derive(Debug, PartialEq)]
    struct Foo {
        location: usize,
    }

    impl Error for Foo {
        fn diagnostic(&self, _: usize) -> Diagnostic {
            Diagnostic::bug()
        }

        fn span(&self) -> Span {
            Span {
                start: self.location,
                end: self.location + 1,
            }
        }

        fn as_any(&self) -> &dyn std::any::Any {
            self
        }
    }

    #[derive(Debug, PartialEq)]
    struct Bar {}

    impl Error for Bar {
        fn diagnostic(&self, _: usize) -> Diagnostic {
            Diagnostic::bug()
        }

        fn span(&self) -> Span {
            0..0
        }

        fn as_any(&self) -> &dyn std::any::Any {
            self
        }
    }
}
