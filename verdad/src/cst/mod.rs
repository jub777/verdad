mod expression;
mod function;
mod inductive;
mod visitor;

use crate::{Span, StringId};

pub use self::function::Function;
pub use expression::{Arm, Call, Expression, Match, Pattern};
pub use inductive::{Constructor, InductiveDefinition};
pub use visitor::*;

#[derive(Debug, Eq, PartialEq)]
pub struct Cst {
    pub objects: Vec<Object>,
}

impl Cst {
    pub fn inductives(&self) -> impl Iterator<Item = &InductiveDefinition> {
        self.objects.iter().flat_map(|object| match object {
            Object::Inductive(inductive) => Some(inductive),
            _ => None,
        })
    }

    pub fn functions(&self) -> impl Iterator<Item = &Function> {
        self.objects.iter().flat_map(|object| match object {
            Object::Function(function) => Some(function),
            _ => None,
        })
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Object {
    Inductive(InductiveDefinition),
    Function(Function),
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Ident {
    pub symbol: StringId,
    pub span: Span,
}

impl Ident {
    pub fn new(start: usize, string: &str) -> Self {
        Self {
            symbol: StringId::new(string),
            span: Span {
                start,
                end: start + string.len(),
            },
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Argument {
    pub name: Ident,
    pub class: Ident,
}
