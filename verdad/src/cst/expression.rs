use crate::Span;

use super::Ident;

#[derive(PartialEq, Debug, Eq)]
pub enum Expression {
    Call(Call),
    Match(Match),
}

#[derive(PartialEq, Debug, Eq)]
pub struct Call {
    pub name: Ident,
    pub arguments: Option<Vec<Expression>>,
}

#[derive(PartialEq, Debug, Eq)]
pub struct Match {
    pub argument: Ident,
    pub arms: Vec<Arm>,
    pub span: Span,
}

#[derive(PartialEq, Debug, Eq)]
pub struct Arm {
    pub pattern: Pattern,
    pub expression: Expression,
}

#[derive(PartialEq, Debug, Eq)]
pub struct Pattern {
    pub name: Ident,
    pub bindings: Vec<Ident>,
}
