use super::{
    expression::{Match, Pattern},
    Argument, Arm, Call, Constructor, Cst, Expression, Function, Ident, InductiveDefinition,
    Object,
};

pub trait Visitor: Sized {
    type Output;
    type Error;

    fn visit(mut self, cst: &Cst) -> Result<Self::Output, Self::Error> {
        self.visit_cst(cst)?;
        self.finish()
    }

    fn visit_cst(&mut self, cst: &Cst) -> Result<(), Self::Error> {
        walk_cst(self, cst)
    }

    fn visit_inductive(&mut self, inductive: &InductiveDefinition) -> Result<(), Self::Error> {
        walk_inductive(self, inductive)
    }

    fn visit_function(&mut self, function: &Function) -> Result<(), Self::Error> {
        walk_function(self, function)
    }

    fn visit_expression(&mut self, expression: &Expression) -> Result<(), Self::Error> {
        walk_expression(self, expression)
    }

    fn visit_argument(&mut self, _: &Argument) -> Result<(), Self::Error> {
        Ok(())
    }

    fn visit_return_class(&mut self, _: &Ident) -> Result<(), Self::Error> {
        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &Constructor) -> Result<(), Self::Error> {
        walk_constructor(self, constructor)
    }

    fn visit_call(&mut self, call: &Call) -> Result<(), Self::Error> {
        walk_call_arguments(self, call)
    }

    fn visit_match(&mut self, match_instance: &Match) -> Result<(), Self::Error> {
        walk_match(self, match_instance)?;
        Ok(())
    }

    fn visit_match_argument(&mut self, _: &Ident) -> Result<(), Self::Error> {
        Ok(())
    }

    fn visit_arm(&mut self, arm: &Arm) -> Result<(), Self::Error> {
        walk_arm(self, arm)?;
        Ok(())
    }

    fn visit_pattern(&mut self, pattern: &Pattern) -> Result<(), Self::Error> {
        walk_pattern(self, pattern)?;
        Ok(())
    }

    fn visit_binding(&mut self, _: &Ident) -> Result<(), Self::Error> {
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error>;
}

pub fn walk_cst<V: Visitor>(visitor: &mut V, cst: &Cst) -> Result<(), V::Error> {
    for object in cst.objects.iter() {
        match object {
            Object::Inductive(inductive) => visitor.visit_inductive(inductive)?,
            Object::Function(function) => visitor.visit_function(function)?,
        }
    }
    Ok(())
}

pub fn walk_inductive<V: Visitor>(
    visitor: &mut V,
    inductive: &InductiveDefinition,
) -> Result<(), V::Error> {
    walk_inductive_arguments(visitor, inductive)?;
    walk_inductive_constructors(visitor, inductive)?;
    Ok(())
}

pub fn walk_inductive_constructors<V: Visitor>(
    visitor: &mut V,
    inductive: &InductiveDefinition,
) -> Result<(), V::Error> {
    for constructor in inductive.constructors.iter() {
        visitor.visit_constructor(constructor)?;
    }
    Ok(())
}

pub fn walk_inductive_arguments<V: Visitor>(
    visitor: &mut V,
    inductive: &InductiveDefinition,
) -> Result<(), V::Error> {
    for argument in inductive.arguments.iter() {
        visitor.visit_argument(argument)?;
    }
    Ok(())
}

pub fn walk_constructor<V: Visitor>(
    visitor: &mut V,
    constructor: &Constructor,
) -> Result<(), V::Error> {
    walk_constructor_arguments(visitor, constructor)?;
    walk_constructor_class(visitor, constructor)?;
    Ok(())
}

pub fn walk_constructor_arguments<V: Visitor>(
    visitor: &mut V,
    constructor: &Constructor,
) -> Result<(), V::Error> {
    for argument in constructor.arguments.iter() {
        visitor.visit_argument(argument)?;
    }
    Ok(())
}

pub fn walk_constructor_class<V: Visitor>(
    visitor: &mut V,
    constructor: &Constructor,
) -> Result<(), V::Error> {
    if let Some(class) = &constructor.class {
        visitor.visit_call(class)?;
    }
    Ok(())
}

pub fn walk_function<V: Visitor>(visitor: &mut V, function: &Function) -> Result<(), V::Error> {
    walk_function_arguments(visitor, function)?;
    visitor.visit_return_class(&function.return_class)?;
    visitor.visit_expression(&function.body)?;
    Ok(())
}

pub fn walk_function_arguments<V: Visitor>(
    visitor: &mut V,
    function: &Function,
) -> Result<(), V::Error> {
    for argument in function.arguments.iter() {
        visitor.visit_argument(argument)?;
    }
    Ok(())
}

pub fn walk_expression<V: Visitor>(
    visitor: &mut V,
    expression: &Expression,
) -> Result<(), V::Error> {
    match expression {
        Expression::Call(call) => visitor.visit_call(call),
        Expression::Match(match_definition) => visitor.visit_match(match_definition),
    }
}

pub fn walk_call_arguments<V: Visitor>(visitor: &mut V, call: &Call) -> Result<(), V::Error> {
    if let Some(arguments) = &call.arguments {
        for argument in arguments.iter() {
            visitor.visit_expression(argument)?;
        }
    }
    Ok(())
}

pub fn walk_match<V: Visitor>(visitor: &mut V, match_instance: &Match) -> Result<(), V::Error> {
    visitor.visit_match_argument(&match_instance.argument)?;
    for arm in match_instance.arms.iter() {
        visitor.visit_arm(arm)?;
    }
    Ok(())
}

pub fn walk_arm<V: Visitor>(visitor: &mut V, arm: &Arm) -> Result<(), V::Error> {
    visitor.visit_pattern(&arm.pattern)?;
    visitor.visit_expression(&arm.expression)?;
    Ok(())
}

pub fn walk_pattern<V: Visitor>(visitor: &mut V, binding: &Pattern) -> Result<(), V::Error> {
    for binding in binding.bindings.iter() {
        visitor.visit_binding(binding)?;
    }
    Ok(())
}
