use super::{Argument, Call, Ident};

#[derive(Debug, Eq, PartialEq)]
pub struct InductiveDefinition {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub constructors: Vec<Constructor>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Constructor {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub class: Option<Call>,
}
