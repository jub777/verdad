use super::{Argument, Expression, Ident};

#[derive(Debug, PartialEq, Eq)]
pub struct Function {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub return_class: Ident,
    pub body: Expression,
}
