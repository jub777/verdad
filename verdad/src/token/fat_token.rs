use super::{Keyword, Span, Symbol};

pub use crate::interner::StringId;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct FatToken {
    pub kind: FatTokenKind,
    pub span: Span,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FatTokenKind {
    Keyword(Keyword),
    Whitespace,
    Name(StringId),
    Unknown(StringId),
    Symbol(Symbol),
    EndOfFile,
}

impl FatTokenKind {
    pub fn is_name(&self) -> bool {
        matches!(self, FatTokenKind::Name(_))
    }

    pub fn symbol(&self) -> Option<StringId> {
        match self {
            Self::Name(symbol) | Self::Unknown(symbol) => Some(*symbol),
            _ => None,
        }
    }
}

impl From<Symbol> for FatTokenKind {
    fn from(symbol: Symbol) -> Self {
        Self::Symbol(symbol)
    }
}

impl From<Keyword> for FatTokenKind {
    fn from(keyword: Keyword) -> Self {
        Self::Keyword(keyword)
    }
}
