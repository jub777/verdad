use super::{Keyword, Span, Symbol};

#[derive(Debug, PartialEq, Eq)]
pub struct ThinToken {
    pub span: Span,
    pub kind: ThinTokenKind,
}

impl ThinToken {
    pub fn is_not_whitespace(&self) -> bool {
        self.kind != ThinTokenKind::Whitespace
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum ThinTokenKind {
    Keyword(Keyword),
    Whitespace,
    Name,
    Unknown,
    Symbol(Symbol),
    EndOfFile,
}

impl From<Symbol> for ThinTokenKind {
    fn from(symbol: Symbol) -> Self {
        Self::Symbol(symbol)
    }
}

impl From<Keyword> for ThinTokenKind {
    fn from(keyword: Keyword) -> Self {
        Self::Keyword(keyword)
    }
}
