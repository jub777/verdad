mod fat_token;
mod thin_token;

pub use fat_token::{FatToken, FatTokenKind};
pub use thin_token::{ThinToken, ThinTokenKind};

pub type Span = std::ops::Range<usize>;

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Keyword {
    Ind,
    Fn,
    Match,
    Prop,
}

impl Keyword {
    pub const fn as_str(&self) -> &'static str {
        match self {
            Self::Ind => "ind",
            Self::Fn => "fn",
            Self::Match => "match",
            Self::Prop => "prop",
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Symbol {
    OpenParen,
    CloseParen,
    OpenBrace,
    CloseBrace,
    Comma,
    Colon,
    Arrow,
}

impl Symbol {
    pub const fn as_str(&self) -> &'static str {
        match self {
            Self::OpenParen => "(",
            Self::CloseParen => ")",
            Self::OpenBrace => "{",
            Self::CloseBrace => "}",
            Self::Comma => ",",
            Self::Colon => ":",
            Self::Arrow => "->",
        }
    }

    pub const fn english_name(&self) -> &'static str {
        match self {
            Self::OpenParen => "open parenthesis",
            Self::CloseParen => "close parenthesis",
            Self::OpenBrace => "open brace",
            Self::CloseBrace => "close brace",
            Self::Comma => "comma",
            Self::Colon => "colon",
            Self::Arrow => "arrow",
        }
    }
}
