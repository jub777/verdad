mod cst_formatter;
mod format;
mod types;

use crate::cst::{Cst, Visitor};
use cst_formatter::CstFormatter;

pub fn cst_to_str(cst: &Cst) -> String {
    CstFormatter::new().visit(cst).unwrap()
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    fn format(source: &str) -> String {
        let cst = crate::str_to_cst(source).unwrap();
        cst_to_str(&cst)
    }

    #[test]
    fn format_constructor_with_inductive_call() {
        assert_eq!(
            format("ind Nil{} ind Eq(a: Nil, b: Nill){Reflex(c: Nil): Eq(c, c)}"),
            "ind Nil {}\n\nind Eq(a: Nil, b: Nill) {\n    Reflex(c: Nil): Eq(c, c),\n}\n"
        );
    }

    #[test]
    fn format_variable_reference() {
        assert_eq!(
            format("ind Never{}fn foo(a:Never)->Never {a}"),
            "ind Never {}\n\nfn foo(a: Never) -> Never {\n    a\n}\n"
        );
    }

    #[test]
    fn format_empty_cst_to_empty_string() {
        assert_eq!(format(""), "");
    }

    #[test]
    fn format_single_empty_inductive_definition() {
        assert_eq!(format("ind   Unit\n{}"), "ind Unit {}\n");
    }

    #[test]
    fn format_single_simple_inductive_definition() {
        assert_eq!(
            format("ind   Bool\n\n{True,False}"),
            "ind Bool {\n    True,\n    False,\n}\n"
        );
    }

    #[test]
    fn format_two_simple_inductive_definitions() {
        assert_eq!(
            format("ind   Bool\n\n{True,False} ind Bits{Zero,One}"),
            "ind Bool {\n    True,\n    False,\n}\n\nind Bits {\n    Zero,\n    One,\n}\n"
        );
    }

    #[test]
    fn format_single_simple_function_definition() {
        assert_eq!(
            format("fn   drop\n\n(a:  Bool)  ->Bool{False   \n}"),
            "fn drop(a: Bool) -> Bool {\n    False\n}\n"
        );
    }

    #[test]
    fn format_function_definition_with_lots_of_arguments_as_multiline() {
        let variable = "x".repeat(40);
        let source = format!(
            "fn   drop\n\n({}:Bool,{}:Bool)  ->Bool{{False   \n}}",
            variable, variable
        );
        assert_eq!(
            format(&source),
            format!(
                "fn drop(\n    {}: Bool,\n    {}: Bool\n) -> Bool {{\n    False\n}}\n",
                variable, variable
            )
        );
    }

    #[test]
    fn format_trivial_match_in_function_definition() {
        assert_eq!(
            format("fn never(a: Never)->Never{match a{}}"),
            "fn never(a: Never) -> Never {\n    match a {}\n}\n"
        );
    }

    #[test]
    fn format_match_with_pattern_binding() {
        assert_eq!(
            format("fn never()->Foo{match a{Bar(b)->Baz}}"),
            "fn never() -> Foo {\n    match a {\n        Bar(b) -> Baz,\n    }\n}\n"
        );
    }

    #[test]
    fn format_simple_match_in_function_definition() {
        assert_eq!(
            format("fn not(a:Bool)->Bool{match a{True->False,False->True}}"),
            "fn not(a: Bool) -> Bool {\n    match a {\n        True -> False,\n        False -> True,\n    }\n}\n"
        );
    }

    #[test]
    fn format_explicit_constructor_type() {
        assert_eq!(
            format("ind Bool {True: Bool}"),
            "ind Bool {\n    True: Bool,\n}\n"
        );
    }

    #[test]
    fn format_constructor_with_arguments() {
        assert_eq!(
            format("ind Int {Zero, Inc(n: Int)} fn one()->Int{Inc(Zero)}"),
            "ind Int {\n    Zero,\n    Inc(n: Int),\n}\n\nfn one() -> Int {\n    Inc(Zero)\n}\n"
        );
    }

    #[test]
    fn format_inductive_with_arguments() {
        assert_eq!(
            format("ind  Equal(a:Bool, b: Bool)  {}"),
            "ind Equal(a: Bool, b: Bool) {}\n"
        );
    }

    #[test]
    fn format_inductive_with_empty_argument_parenthesis() {
        assert_eq!(format("ind  Equal()  {}"), "ind Equal {}\n");
    }
}
