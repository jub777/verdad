use super::types::{Item, LineMode, Separator, Sequence};
use std::fmt::{Display, Error, Formatter};

const MAX_LINE_LENGTH: usize = 80;

impl<'a> Display for Sequence<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        self.format(f, 0)
    }
}

impl<'a> Sequence<'a> {
    fn formatted_string_length(&self) -> usize {
        let items_length: usize = self.items.iter().map(Item::formatted_string_length).sum();
        let separator_length = self.separator.formatted_string_length(self.items.len());
        let space_length = self.items.len().saturating_sub(1);
        self.prefix.len() + items_length + separator_length + space_length + self.suffix.len()
    }

    fn format(&self, f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
        let length = indent_len(depth) + self.formatted_string_length();
        match self.line_mode {
            LineMode::Single => self.format_single_line(f, depth),
            LineMode::Minimize if length <= MAX_LINE_LENGTH => self.format_single_line(f, depth),
            LineMode::Minimize | LineMode::MultiIndented => self.format_multi_indented(f, depth),
            LineMode::MultiUnindented => self.format_multi_unindented(f, depth),
        }
    }

    fn format_single_line(&self, f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
        write!(f, "{}", &self.prefix)?;
        for (index, item) in self.items.iter().enumerate() {
            item.format(f, depth)?;
            let is_last = index == self.items.len() - 1;
            self.separator.format_single_line(f, is_last)?;
        }
        write!(f, "{}", self.suffix)?;
        Ok(())
    }

    fn format_multi_indented(&self, f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
        write!(f, "{}", &self.prefix)?;
        writeln!(f)?;
        for (index, item) in self.items.iter().enumerate() {
            write_indent(f, depth + 1)?;
            item.format(f, depth + 1)?;
            let is_last = index == self.items.len() - 1;
            self.separator.format_multi_line(f, is_last)?;
        }
        write_with_indent(f, &self.suffix, depth)?;
        Ok(())
    }

    fn format_multi_unindented(&self, f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
        write!(f, "{}", &self.prefix)?;
        for (index, item) in self.items.iter().enumerate() {
            item.format(f, depth)?;
            let is_last = index == self.items.len() - 1;
            self.separator.format_multi_line(f, is_last)?;
        }
        write!(f, "{}", &self.suffix)?;
        Ok(())
    }
}

impl Separator {
    fn formatted_string_length(&self, items: usize) -> usize {
        match self {
            Self::Trailing(string) => items * string.len(),
            Self::Intersperse(string) => items.saturating_sub(1) * string.len(),
        }
    }

    fn format_single_line(&self, f: &mut Formatter<'_>, is_last: bool) -> Result<(), Error> {
        match self {
            Self::Trailing(string) => write!(f, "{}", string)?,
            Self::Intersperse(string) if !is_last => write!(f, "{}", string)?,
            _ => {}
        }
        if !is_last {
            write!(f, " ")?;
        }
        Ok(())
    }

    fn format_multi_line(&self, f: &mut Formatter<'_>, is_last: bool) -> Result<(), Error> {
        match self {
            Self::Trailing(string) => write!(f, "{}", string)?,
            Self::Intersperse(string) if !is_last => write!(f, "{}", string)?,
            _ => {}
        }
        writeln!(f)?;
        Ok(())
    }
}

impl<'a> Item<'a> {
    fn formatted_string_length(&self) -> usize {
        match self {
            Self::String(string) => string.len(),
            Self::Sequence(sequence) => sequence.formatted_string_length(),
        }
    }

    fn format(&self, f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
        match self {
            Self::String(string) => write!(f, "{}", string),
            Self::Sequence(sequence) => sequence.format(f, depth),
        }
    }
}

fn write_with_indent(f: &mut Formatter<'_>, string: &str, depth: usize) -> Result<(), Error> {
    write!(f, "{:indent$}{}", "", string, indent = indent_len(depth))
}

fn write_indent(f: &mut Formatter<'_>, depth: usize) -> Result<(), Error> {
    write!(f, "{:indent$}", "", indent = indent_len(depth))
}

fn indent_len(depth: usize) -> usize {
    const SPACES_PER_INDENT: usize = 4;
    depth * SPACES_PER_INDENT
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::formatter::types::Stack;

    #[test]
    fn empty_sequence_to_an_empty_string() {
        assert_eq!(
            Stack::new("", Separator::Intersperse(""), "", LineMode::Minimize).finish(),
            String::new()
        );
    }

    #[test]
    fn short_constructor_to_single_line() {
        let mut stack = Stack::new("S(", Separator::Intersperse(","), ")", LineMode::Minimize);
        stack.add_string("n: Nat");
        assert_eq!(stack.finish(), "S(n: Nat)");
    }

    #[test]
    fn short_multiple_items_constructor_to_single_line() {
        let mut stack = Stack::new(
            "Tuple(",
            Separator::Intersperse(","),
            ")",
            LineMode::Minimize,
        );
        stack.add_string("n: Nat");
        stack.add_string("n: Nat");
        stack.add_string("n: Nat");
        assert_eq!(stack.finish(), "Tuple(n: Nat, n: Nat, n: Nat)");
    }

    #[test]
    fn long_multiple_item_constructor_to_multi_line() {
        let mut stack = Stack::new(
            "Pair(",
            Separator::Intersperse(","),
            ")",
            LineMode::Minimize,
        );
        let variable = "x".repeat(MAX_LINE_LENGTH / 2);
        stack.add_string(format!("{}: Nat", variable));
        stack.add_string(format!("{}: Nat", variable));
        assert_eq!(
            stack.finish(),
            format!("Pair(\n    {}: Nat,\n    {}: Nat\n)", variable, variable)
        );
    }

    #[test]
    fn long_multiple_item_constructor_to_single_line() {
        let mut stack = Stack::new("Pair(", Separator::Intersperse(","), ")", LineMode::Single);
        let variable = "x".repeat(MAX_LINE_LENGTH / 2);
        stack.add_string(format!("{}: Nat", variable));
        stack.add_string(format!("{}: Nat", variable));
        assert_eq!(
            stack.finish(),
            format!("Pair({}: Nat, {}: Nat)", variable, variable)
        );
    }

    #[test]
    fn even_short_inductives_to_multi_line() {
        let mut stack = Stack::new(
            "ind bool {",
            Separator::Trailing(","),
            "}",
            LineMode::MultiIndented,
        );
        stack.add_string("true");
        stack.add_string("false");
        assert_eq!(stack.finish(), "ind bool {\n    true,\n    false,\n}");
    }

    #[test]
    fn inductives_with_arguments_to_multi_line() {
        let mut stack = Stack::new(
            "ind Option {",
            Separator::Trailing(","),
            "}",
            LineMode::MultiIndented,
        );
        stack.add_string("None");
        stack.start_sequence(
            "Some(",
            Separator::Intersperse(","),
            ")",
            LineMode::Minimize,
        );
        stack.add_string("int_that_is_verry_verry_long: Nat");
        stack.add_string("int_that_is_verry_verry_long: Nat");
        stack.add_string("int_that_is_verry_verry_long: Nat");
        stack.finish_sequence();
        assert_eq!(
            stack.finish(),
            "ind Option {\n    None,\n    Some(\n        int_that_is_verry_verry_long: Nat,\n        int_that_is_verry_verry_long: Nat,\n        int_that_is_verry_verry_long: Nat\n    ),\n}"
        );
    }

    #[test]
    fn minimal_multi_line_constructor() {
        let mut stack = Stack::new("S(", Separator::Intersperse(","), ")", LineMode::Minimize);
        stack.add_string(
            "this_variable_is_unnessassarriaasaaaaaaasrrily_long_but_will_prove_a_point: Nat",
        );
        assert_eq!(
            stack.finish(),
            "S(\n    this_variable_is_unnessassarriaasaaaaaaasrrily_long_but_will_prove_a_point: Nat\n)"
        );
    }

    #[test]
    fn indent_used_in_line_length_check() {
        let variable = format!("{}: Nat", "x".repeat(70));
        let mut stack = Stack::new(
            "ind Nat {",
            Separator::Trailing(","),
            "}",
            LineMode::MultiIndented,
        );
        stack.start_sequence("S(", Separator::Intersperse(","), ")", LineMode::Minimize);
        stack.add_string(variable.clone());
        stack.finish_sequence();
        assert_eq!(
            stack.finish(),
            format!("ind Nat {{\n    S(\n        {}\n    ),\n}}", variable)
        );
    }

    #[test]
    fn multiple_inductives() {
        let mut stack = Stack::new(
            "",
            Separator::Intersperse("\n"),
            "",
            LineMode::MultiUnindented,
        );
        stack.start_sequence(
            "ind Empty {",
            Separator::Intersperse(","),
            "}",
            LineMode::Minimize,
        );
        stack.finish_sequence();
        stack.start_sequence(
            "ind Unit {",
            Separator::Intersperse(","),
            "}",
            LineMode::Minimize,
        );
        stack.finish_sequence();
        assert_eq!(stack.finish(), "ind Empty {}\n\nind Unit {}\n");
    }

    #[test]
    fn format_match_arms() {
        let mut stack = Stack::new(
            "match a {",
            Separator::Trailing(","),
            "}",
            LineMode::MultiIndented,
        );
        stack.start_sequence("", Separator::Intersperse(" ->"), "", LineMode::Single);
        stack.add_string("True");
        stack.add_string("False");
        stack.finish_sequence();
        stack.start_sequence("", Separator::Intersperse(" ->"), "", LineMode::Single);
        stack.add_string("False");
        stack.add_string("True");
        stack.finish_sequence();
        assert_eq!(
            stack.finish(),
            "match a {\n    True -> False,\n    False -> True,\n}"
        );
    }
}
