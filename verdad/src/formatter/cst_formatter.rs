use super::types::{LineMode, Separator, Stack};
use crate::cst::{Arm, Constructor, Cst, Function, InductiveDefinition};

pub struct CstFormatter<'i> {
    stack: Stack<'i>,
}

impl<'i> CstFormatter<'i> {
    pub fn new() -> Self {
        CstFormatter {
            stack: Stack::new(
                "",
                Separator::Intersperse("\n"),
                "",
                LineMode::MultiUnindented,
            ),
        }
    }
}

impl<'i> crate::cst::Visitor for CstFormatter<'i> {
    type Output = String;
    type Error = ();

    fn visit_cst(&mut self, cst: &Cst) -> Result<(), ()> {
        crate::cst::walk_cst(self, cst)?;
        Ok(())
    }

    fn visit_function(&mut self, function: &Function) -> Result<(), ()> {
        self.stack
            .start_sequence("", Separator::Intersperse(""), "", LineMode::Single);

        // Arguments
        let name = &function.name.symbol;
        let return_class = &function.return_class.symbol;
        self.stack.start_sequence(
            format!("fn {}(", name),
            Separator::Intersperse(","),
            format!(") -> {}", return_class),
            LineMode::Minimize,
        );
        crate::cst::walk_function_arguments(self, function)?;
        self.stack.finish_sequence();

        // Body
        self.stack.start_sequence(
            "{",
            Separator::Intersperse(""),
            "}",
            LineMode::MultiIndented,
        );
        crate::cst::walk_expression(self, &function.body)?;
        self.stack.finish_sequence();

        self.stack.finish_sequence();

        Ok(())
    }

    fn visit_argument(&mut self, argument: &crate::cst::Argument) -> Result<(), ()> {
        let name = &argument.name.symbol;
        let class = &argument.class.symbol;
        self.stack.add_string(format!("{}: {}", name, class));
        Ok(())
    }

    fn visit_call(&mut self, call: &crate::cst::Call) -> Result<(), ()> {
        let name = &call.name.symbol;
        let no_arguments = call.arguments.as_ref().map(Vec::is_empty).unwrap_or(true);
        if no_arguments {
            self.stack.add_string(name.as_str());
        } else {
            self.stack.start_sequence(
                format!("{}(", name),
                Separator::Intersperse(","),
                ")",
                LineMode::Minimize,
            );
            crate::cst::walk_call_arguments(self, call)?;
            self.stack.finish_sequence();
        }
        Ok(())
    }

    fn visit_match(&mut self, match_definition: &crate::cst::Match) -> Result<(), ()> {
        let line_mode = match match_definition.arms.is_empty() {
            true => LineMode::Minimize,
            false => LineMode::MultiIndented,
        };
        let argument = &match_definition.argument.symbol;
        self.stack.start_sequence(
            format!("match {} {{", argument),
            Separator::Trailing(","),
            "}",
            line_mode,
        );
        crate::cst::walk_match(self, match_definition)?;
        self.stack.finish_sequence();
        Ok(())
    }

    fn visit_arm(&mut self, arm: &Arm) -> Result<(), Self::Error> {
        self.stack
            .start_sequence("", Separator::Intersperse(" ->"), "", LineMode::Single);
        self.visit_pattern(&arm.pattern)?;
        self.visit_expression(&arm.expression)?;
        self.stack.finish_sequence();
        Ok(())
    }

    fn visit_pattern(&mut self, pattern: &crate::cst::Pattern) -> Result<(), ()> {
        let name = &pattern.name.symbol;
        if pattern.bindings.is_empty() {
            self.stack.add_string(name.as_str());
        } else {
            self.stack.start_sequence(
                format!("{}(", name),
                Separator::Intersperse(","),
                ")",
                LineMode::Minimize,
            );
            crate::cst::walk_pattern(self, pattern)?;
            self.stack.finish_sequence();
        }
        Ok(())
    }

    fn visit_binding(&mut self, binding: &crate::cst::Ident) -> Result<(), Self::Error> {
        self.stack.add_string(binding.symbol.as_str());
        Ok(())
    }

    fn visit_inductive(&mut self, inductive: &InductiveDefinition) -> Result<(), ()> {
        self.stack
            .start_sequence("", Separator::Intersperse(""), "", LineMode::Single);

        // Arguments
        let name = &inductive.name.symbol;
        if inductive.arguments.is_empty() {
            self.stack.add_string(format!("ind {}", name));
        } else {
            self.stack.start_sequence(
                format!("ind {}(", name),
                Separator::Intersperse(","),
                ")",
                LineMode::Minimize,
            );
            crate::cst::walk_inductive_arguments(self, inductive)?;
            self.stack.finish_sequence();
        }

        // Constructors
        let line_mode = match inductive.constructors.is_empty() {
            true => LineMode::Minimize,
            false => LineMode::MultiIndented,
        };
        self.stack
            .start_sequence("{", Separator::Trailing(","), "}", line_mode);
        crate::cst::walk_inductive_constructors(self, inductive)?;
        self.stack.finish_sequence();

        self.stack.finish_sequence();

        Ok(())
    }

    fn visit_constructor(&mut self, constructor: &Constructor) -> Result<(), ()> {
        self.stack
            .start_sequence("", Separator::Intersperse(":"), "", LineMode::Single);

        // Name and Arguments
        let name = &constructor.name.symbol;
        if constructor.arguments.is_empty() {
            self.stack.add_string(name.as_str());
        } else {
            self.stack.start_sequence(
                format!("{}(", name),
                Separator::Intersperse(","),
                ")",
                LineMode::Minimize,
            );
            crate::cst::walk_constructor_arguments(self, constructor)?;
            self.stack.finish_sequence();
        }

        // Constructor Call
        crate::cst::walk_constructor_class(self, constructor)?;

        self.stack.finish_sequence();

        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error> {
        Ok(self.stack.finish())
    }
}
