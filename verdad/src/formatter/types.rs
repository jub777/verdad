use std::borrow::Cow;

pub struct Stack<'a> {
    top: Sequence<'a>,
    sequences: Vec<Sequence<'a>>,
}

impl<'a> Stack<'a> {
    pub fn new<P, S>(prefix: P, separator: Separator, suffix: S, line_mode: LineMode) -> Self
    where
        P: Into<Cow<'a, str>>,
        S: Into<Cow<'a, str>>,
    {
        Self {
            top: Sequence {
                prefix: prefix.into(),
                items: vec![],
                separator,
                suffix: suffix.into(),
                line_mode,
            },
            sequences: vec![],
        }
    }

    pub fn add_string<S>(&mut self, string: S)
    where
        S: Into<Cow<'a, str>>,
    {
        let current = self.sequences.last_mut().unwrap_or(&mut self.top);
        current.items.push(Item::String(string.into()));
    }

    pub fn start_sequence<P, S>(
        &mut self,
        prefix: P,
        separator: Separator,
        suffix: S,
        line_mode: LineMode,
    ) where
        P: Into<Cow<'a, str>>,
        S: Into<Cow<'a, str>>,
    {
        self.sequences.push(Sequence {
            prefix: prefix.into(),
            items: vec![],
            separator,
            suffix: suffix.into(),
            line_mode,
        });
    }

    pub fn finish_sequence(&mut self) {
        let finished = self.sequences.pop().expect("No sequence to finish");
        let current = self.sequences.last_mut().unwrap_or(&mut self.top);
        current.items.push(Item::Sequence(finished));
    }

    pub fn finish(self) -> String {
        assert_eq!(self.sequences.len(), 0, "Unfinished sequence(s)");
        self.top.to_string()
    }
}

#[derive(Clone)]
pub struct Sequence<'a> {
    pub prefix: Cow<'a, str>,
    pub items: Vec<Item<'a>>,
    pub separator: Separator,
    pub suffix: Cow<'a, str>,
    pub line_mode: LineMode,
}

#[derive(Eq, PartialEq, Clone)]
pub enum LineMode {
    Single,
    Minimize,
    MultiIndented,
    MultiUnindented,
}

#[derive(Eq, PartialEq, Clone)]
pub enum Separator {
    Trailing(&'static str),
    Intersperse(&'static str),
}

#[derive(Clone)]
pub enum Item<'a> {
    String(Cow<'a, str>),
    Sequence(Sequence<'a>),
}
