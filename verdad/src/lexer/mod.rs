mod parse;

use crate::token::{ThinToken, ThinTokenKind};

pub fn str_to_tokens(source: &str) -> impl Iterator<Item = ThinToken> + '_ {
    let mut input = source.into();
    let mut end = false;
    std::iter::from_fn(move || {
        if end {
            return None;
        }
        let (remainder, token) = parse::token(input).unwrap();
        end = token.kind == ThinTokenKind::EndOfFile;
        input = remainder;
        Some(token)
    })
}

#[cfg(test)]
mod tests {
    use crate::token::{Keyword::*, Symbol::*, ThinTokenKind::*};

    use super::*;

    #[test]
    fn test_parse_empty_str_to_just_end_of_file() {
        assert_eq!(
            str_to_tokens("").collect::<Vec<_>>(),
            tokens([(0, EndOfFile)])
        );
    }

    #[test]
    fn test_parse_empty_inductive_definition_tokens() {
        assert_eq!(
            str_to_tokens("ind Bool {True, False}").collect::<Vec<_>>(),
            tokens([
                (3, Ind.into()),
                (1, Whitespace),
                (4, Name),
                (1, Whitespace),
                (1, OpenBrace.into()),
                (4, Name),
                (1, Comma.into()),
                (1, Whitespace),
                (5, Name),
                (1, CloseBrace.into()),
                (0, EndOfFile)
            ])
        );
    }

    #[test]
    fn test_parse_str_of_unexpected_token_errors() {
        assert_eq!(
            str_to_tokens("*").collect::<Vec<_>>(),
            tokens([(1, Unknown), (0, EndOfFile)])
        );
    }

    #[test]
    fn test_parse_trivial_function() {
        assert_eq!(
            str_to_tokens("fn something(a:Never)->Never{match a{}}").collect::<Vec<_>>(),
            tokens([
                (2, Fn.into()),
                (1, Whitespace),
                (9, Name),
                (1, OpenParen.into()),
                (1, Name),
                (1, Colon.into()),
                (5, Name),
                (1, CloseParen.into()),
                (2, Arrow.into()),
                (5, Name),
                (1, OpenBrace.into()),
                (5, Match.into()),
                (1, Whitespace),
                (1, Name),
                (1, OpenBrace.into()),
                (1, CloseBrace.into()),
                (1, CloseBrace.into()),
                (0, EndOfFile)
            ])
        );
    }

    #[test]
    fn names_can_have_keywords_in_them_if_only_partial_match() {
        assert_eq!(
            str_to_tokens("matches").collect::<Vec<_>>(),
            tokens([(7, Name), (0, EndOfFile)])
        );
        assert_eq!(
            str_to_tokens("unmatch").collect::<Vec<_>>(),
            tokens([(7, Name), (0, EndOfFile)])
        );
        assert_eq!(
            str_to_tokens("match").collect::<Vec<_>>(),
            tokens([(5, Match.into()), (0, EndOfFile)])
        );
    }

    #[test]
    fn parse_proposition() {
        assert_eq!(
            str_to_tokens("prop Eq(){}").collect::<Vec<_>>(),
            tokens([
                (4, Prop.into()),
                (1, Whitespace),
                (2, Name),
                (1, OpenParen.into()),
                (1, CloseParen.into()),
                (1, OpenBrace.into()),
                (1, CloseBrace.into()),
                (0, EndOfFile)
            ])
        );
    }

    fn tokens(items: impl IntoIterator<Item = (usize, ThinTokenKind)>) -> Vec<ThinToken> {
        let mut end = 0;
        items
            .into_iter()
            .map(|(length, kind)| {
                let start = end;
                end += length;
                ThinToken {
                    span: start..end,
                    kind,
                }
            })
            .collect()
    }
}
