use nom::{
    branch::alt,
    bytes::complete::{tag, take_while1, take_while_m_n},
    character::complete::multispace1,
    combinator::{map, not, peek},
    sequence::terminated,
    InputIter, Slice,
};

use crate::token::{Keyword, Span, Symbol, ThinToken, ThinTokenKind};

type InputSpan<'a> = nom_locate::LocatedSpan<&'a str>;
type IResult<'s> = nom::IResult<InputSpan<'s>, ThinToken, ()>;

pub fn token(source: InputSpan) -> IResult {
    alt((
        keyword(Keyword::Ind),
        keyword(Keyword::Fn),
        keyword(Keyword::Match),
        keyword(Keyword::Prop),
        whitespace,
        name,
        symbol(Symbol::OpenParen),
        symbol(Symbol::CloseParen),
        symbol(Symbol::OpenBrace),
        symbol(Symbol::CloseBrace),
        symbol(Symbol::Comma),
        symbol(Symbol::Colon),
        symbol(Symbol::Arrow),
        unknown,
        end_of_file,
    ))(source)
}

fn keyword<'a>(keyword: Keyword) -> impl FnMut(InputSpan<'a>) -> IResult {
    let name_suffix = take_while_m_n(1, 1, is_name_char);
    terminated(
        tag_to_token(keyword.as_str(), ThinTokenKind::Keyword(keyword)),
        peek(not(name_suffix)),
    )
}

fn symbol(symbol: Symbol) -> impl Fn(InputSpan) -> IResult {
    tag_to_token(symbol.as_str(), ThinTokenKind::Symbol(symbol))
}

fn whitespace(source: InputSpan) -> IResult {
    map(multispace1, span_to_token(ThinTokenKind::Whitespace))(source)
}

fn name(source: InputSpan) -> IResult {
    map(
        take_while1(is_name_char),
        span_to_token(ThinTokenKind::Name),
    )(source)
}

fn is_name_char(character: char) -> bool {
    character.is_ascii_alphabetic() || character == '_'
}

fn unknown(source: InputSpan) -> IResult {
    match source.iter_elements().next() {
        Some(_) => IResult::Ok((
            source.slice(1..),
            span_to_token(ThinTokenKind::Unknown)(source.slice(0..1)),
        )),
        None => IResult::Err(nom::Err::Error(())),
    }
}

fn end_of_file(source: InputSpan) -> IResult {
    match source.iter_elements().next() {
        Some(_) => IResult::Err(nom::Err::Error(())),
        None => IResult::Ok((source, span_to_token(ThinTokenKind::EndOfFile)(source))),
    }
}

fn tag_to_token(string: &'static str, kind: ThinTokenKind) -> impl Fn(InputSpan) -> IResult {
    move |source| map(tag(string), span_to_token(kind))(source)
}

fn span_to_token(kind: ThinTokenKind) -> impl Fn(InputSpan) -> ThinToken {
    move |span| ThinToken {
        span: Span {
            start: span.location_offset(),
            end: span.location_offset() + span.len(),
        },
        kind,
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_rest_to_unknown() {
        assert_eq!(
            unknown("*".into()).unwrap().1,
            ThinToken {
                span: 0..1,
                kind: ThinTokenKind::Unknown
            }
        );
    }

    #[test]
    fn test_parse_empty_string_to_end_of_file_token() {
        assert_eq!(
            token("".into()).unwrap().1,
            ThinToken {
                span: 0..0,
                kind: ThinTokenKind::EndOfFile
            }
        );
    }
}
