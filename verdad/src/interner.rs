use crate::token;

pub type StringId = symbol_table::GlobalSymbol;

pub fn thin_to_fat_tokens<'a>(
    source: &'a str,
    tokens: impl Iterator<Item = token::ThinToken> + 'a,
) -> impl Iterator<Item = token::FatToken> + 'a {
    tokens.map(move |token| token::FatToken {
        kind: thin_token_to_fat_token_kind(source, &token),
        span: token.span,
    })
}

fn thin_token_to_fat_token_kind(source: &str, token: &token::ThinToken) -> token::FatTokenKind {
    match token.kind {
        token::ThinTokenKind::Keyword(keyword) => token::FatTokenKind::Keyword(keyword),
        token::ThinTokenKind::Whitespace => token::FatTokenKind::Whitespace,
        token::ThinTokenKind::Name => {
            token::FatTokenKind::Name(StringId::new(&source[token.span.clone()]))
        }
        token::ThinTokenKind::Symbol(symbol) => token::FatTokenKind::Symbol(symbol),
        token::ThinTokenKind::Unknown => {
            token::FatTokenKind::Unknown(StringId::new(&source[token.span.clone()]))
        }
        token::ThinTokenKind::EndOfFile => token::FatTokenKind::EndOfFile,
    }
}

#[cfg(test)]
pub mod test {
    use super::*;

    pub fn str_to_fat_tokens(source: &str) -> Vec<token::FatToken> {
        let thin_tokens =
            crate::lexer::str_to_tokens(source).filter(crate::token::ThinToken::is_not_whitespace);
        thin_to_fat_tokens(source, thin_tokens).collect()
    }
}
