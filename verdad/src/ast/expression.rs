use crate::{cst::Ident, Span};

use super::{function::VariableId, ConstructorId, InductiveId, NamespaceId};

#[derive(Debug, PartialEq, Eq)]
pub enum Expression {
    VariableRef(VariableRef),
    ConstructorCall(Call),
    Match(Match),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Call {
    pub ident: Ident,
    pub namespace_id: NamespaceId,
    pub arguments: Vec<Expression>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Match {
    pub argument: VariableRef,
    pub arms: Vec<Arm>,
    pub span: Span,
}

#[derive(Debug, PartialEq, Eq)]
pub struct VariableRef {
    pub variable_id: VariableId,
    pub ident: Ident,
    pub inductive_id: InductiveId,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Arm {
    pub pattern: BindConstructor,
    pub expression: Expression,
}

#[derive(Debug, PartialEq, Eq)]
pub struct BindConstructor {
    pub ident: Ident,
    pub constructor_id: ConstructorId,
    pub sub_patterns: Vec<BindVariable>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct BindVariable {
    pub variable_id: VariableId,
    pub ident: Ident,
}
