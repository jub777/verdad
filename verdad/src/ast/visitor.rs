use super::{
    Ast, BindConstructor, Call, ConstructorDefinition, Definition, DefinitionNamespace, Expression,
    FunctionDefinition, Match, VariableRef,
};

pub trait Visitor<'ast>: Sized {
    type Output;
    type Error;

    fn visit(mut self, ast: &'ast Ast) -> Result<Self::Output, Self::Error> {
        walk_namespace(&mut self, &ast.definitions)?;
        self.finish()
    }

    fn visit_function(&mut self, function: &'ast FunctionDefinition) -> Result<(), Self::Error> {
        walk_function(self, function)
    }

    fn visit_constructor(
        &mut self,
        constructor: &'ast ConstructorDefinition,
    ) -> Result<(), Self::Error> {
        walk_constructor(self, constructor)
    }

    fn visit_expression(&mut self, expression: &'ast Expression) -> Result<(), Self::Error> {
        walk_expression(self, expression)
    }

    fn visit_variable_ref(&mut self, _: &'ast VariableRef) -> Result<(), Self::Error> {
        Ok(())
    }

    fn visit_call(&mut self, call: &'ast Call) -> Result<(), Self::Error> {
        walk_call(self, call)
    }

    fn visit_match(&mut self, match_definition: &'ast Match) -> Result<(), Self::Error> {
        walk_match(self, match_definition)
    }

    fn visit_bind_constructor(&mut self, _: &'ast BindConstructor) -> Result<(), Self::Error> {
        Ok(())
    }

    fn finish(self) -> Result<Self::Output, Self::Error>;
}

pub fn walk_namespace<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    namespace: &'ast DefinitionNamespace,
) -> Result<(), V::Error> {
    for definition in namespace.iter() {
        match definition {
            Definition::Function(function) => visitor.visit_function(function)?,
            Definition::Constructor(constructor) => visitor.visit_constructor(constructor)?,
            _ => (),
        }
    }
    Ok(())
}

pub fn walk_function<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    function: &'ast FunctionDefinition,
) -> Result<(), V::Error> {
    walk_expression(visitor, &function.body)?;
    Ok(())
}

pub fn walk_constructor<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    constructor: &'ast ConstructorDefinition,
) -> Result<(), V::Error> {
    if let Some(class) = constructor.class.as_ref() {
        visitor.visit_call(class)?;
    }
    Ok(())
}

pub fn walk_expression<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    expression: &'ast Expression,
) -> Result<(), V::Error> {
    match expression {
        Expression::VariableRef(variable_ref) => visitor.visit_variable_ref(variable_ref),
        Expression::ConstructorCall(call) => visitor.visit_call(call),
        Expression::Match(match_definition) => visitor.visit_match(match_definition),
    }
}

pub fn walk_call<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    call: &'ast Call,
) -> Result<(), V::Error> {
    for argument in call.arguments.iter() {
        visitor.visit_expression(argument)?;
    }
    Ok(())
}

pub fn walk_match<'ast, V: Visitor<'ast>>(
    visitor: &mut V,
    match_definition: &'ast Match,
) -> Result<(), V::Error> {
    for arm in match_definition.arms.iter() {
        visitor.visit_bind_constructor(&arm.pattern)?;
        walk_expression(visitor, &arm.expression)?;
    }
    Ok(())
}
