mod expression;
mod function;
mod inductive;
mod namespace;
mod object_id;
mod visitor;

use crate::cst::Ident;

pub use expression::{Arm, BindConstructor, BindVariable, Call, Expression, Match, VariableRef};
pub use function::{Argument, FunctionDefinition, FunctionSignature, VariableId};
pub use inductive::{ConstructorDefinition, ConstructorSignature, Inductive};
pub use namespace::{Definition, DefinitionNamespace, Signature, SignatureNamespace};
pub use object_id::{ConstructorId, FunctionId, InductiveId, NamespaceId};
pub use visitor::*;

#[derive(Debug, PartialEq, Eq)]
pub struct Ast {
    pub definitions: DefinitionNamespace,
}

#[derive(Debug, PartialEq, Eq)]
pub struct TypeRef {
    pub inductive_id: InductiveId,
    pub ident: Ident,
}
