use super::object_id::{ConstructorId, FunctionId, InductiveId};
use super::{
    function::FunctionSignature, inductive::ConstructorSignature, ConstructorDefinition,
    FunctionDefinition, Inductive,
};

pub type Definition = Object<ConstructorDefinition, FunctionDefinition>;
pub type Signature = Object<ConstructorSignature, FunctionSignature>;

pub type DefinitionNamespace = Namespace<ConstructorDefinition, FunctionDefinition>;
pub type SignatureNamespace = Namespace<ConstructorSignature, FunctionSignature>;

#[derive(Debug, PartialEq, Eq)]
pub struct Namespace<C, F>(Vec<Object<C, F>>);

impl<C, F> Default for Namespace<C, F> {
    fn default() -> Self {
        Self(Vec::default())
    }
}

impl<C, F> Namespace<C, F> {
    pub fn add_function(&mut self, function: F) -> FunctionId {
        self.0.push(Object::Function(function));
        FunctionId(self.0.len() - 1)
    }

    pub fn add_inductive(&mut self, inductive: Inductive) -> InductiveId {
        self.0.push(Object::Inductive(inductive));
        InductiveId(self.0.len() - 1)
    }

    pub fn add_constructor(&mut self, constructor: C) -> ConstructorId {
        self.0.push(Object::Constructor(constructor));
        ConstructorId(self.0.len() - 1)
    }

    pub fn iter(&self) -> impl Iterator<Item = &Object<C, F>> + '_ {
        self.0.iter()
    }
}

impl<C, F> std::ops::Index<FunctionId> for Namespace<C, F> {
    type Output = F;

    fn index(&self, index: FunctionId) -> &Self::Output {
        self.0[index.0].unwrap_as_function()
    }
}

impl<C, F> std::ops::Index<InductiveId> for Namespace<C, F> {
    type Output = Inductive;

    fn index(&self, index: InductiveId) -> &Self::Output {
        self.0[index.0].unwrap_as_inductive()
    }
}

impl<C, F> std::ops::Index<ConstructorId> for Namespace<C, F> {
    type Output = C;

    fn index(&self, index: ConstructorId) -> &Self::Output {
        self.0[index.0].unwrap_as_constructor()
    }
}

#[cfg(test)]
impl PartialEq<Vec<Definition>> for DefinitionNamespace {
    fn eq(&self, other: &Vec<Definition>) -> bool {
        &self.0 == other
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Object<C, F> {
    Inductive(Inductive),
    Constructor(C),
    Function(F),
}

impl<C, F> Object<C, F> {
    pub fn unwrap_as_function(&self) -> &F {
        match self {
            Self::Function(function) => function,
            _ => panic!("not a function"),
        }
    }

    pub fn unwrap_as_inductive(&self) -> &Inductive {
        match self {
            Self::Inductive(inductive) => inductive,
            _ => panic!("not an inductive"),
        }
    }

    pub fn unwrap_as_constructor(&self) -> &C {
        match self {
            Self::Constructor(constructor) => constructor,
            _ => panic!("not a constructor"),
        }
    }
}

impl SignatureNamespace {
    pub fn map<F>(self, mapper: F) -> DefinitionNamespace
    where
        F: FnMut(Signature) -> Definition,
    {
        Namespace(self.0.into_iter().map(mapper).collect())
    }
}
