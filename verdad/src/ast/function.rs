use crate::ast::{Expression, TypeRef};
use crate::cst::Ident;

#[derive(Debug, PartialEq, Eq)]
pub struct FunctionDefinition {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub return_class: TypeRef,
    pub body: Expression,
}

pub struct FunctionSignature {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub return_class: TypeRef,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Argument {
    pub name: Ident,
    pub class: TypeRef,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct VariableId(pub usize);
