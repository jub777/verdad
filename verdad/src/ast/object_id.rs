#[derive(Debug, PartialEq, Eq, Clone)]
pub enum NamespaceId {
    Function(FunctionId),
    Inductive(InductiveId),
    Constructor(ConstructorId),
}

impl NamespaceId {
    pub fn unwrap_function(&self) -> FunctionId {
        match self {
            Self::Function(function) => *function,
            _ => panic!("not an function"),
        }
    }

    pub fn unwrap_inductive(&self) -> InductiveId {
        match self {
            Self::Inductive(inductive) => *inductive,
            _ => panic!("not an inductive"),
        }
    }

    pub fn unwrap_constructor(&self) -> ConstructorId {
        match self {
            Self::Constructor(constructor) => *constructor,
            _ => panic!("not an constructor"),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub struct FunctionId(pub usize);

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub struct InductiveId(pub usize);

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub struct ConstructorId(pub usize);
