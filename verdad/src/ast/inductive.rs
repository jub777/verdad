use super::{Argument, Call, ConstructorId, InductiveId};
use crate::cst::Ident;

#[derive(Debug, PartialEq, Eq)]
pub struct Inductive {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub constructor_ids: Vec<ConstructorId>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct ConstructorSignature {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub inductive_id: InductiveId,
}

#[derive(Debug, PartialEq, Eq)]
pub struct ConstructorDefinition {
    pub name: Ident,
    pub arguments: Vec<Argument>,
    pub class: Option<Call>,
}
