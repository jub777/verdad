mod argument;
mod constructor;
mod error;
mod expression;
mod function;
mod ident;
mod inductive;
mod parse;

use nom::branch::alt;
use nom::sequence::terminated;
use nom::{combinator::map, multi::many0, Finish};

use self::parse::{must, try_take_kind, IResult};
use crate::cst::Object;
use crate::token::FatToken;
use crate::{cst::Cst, token::FatTokenKind};
use error::{Error, ErrorKind};

pub fn tokens_to_cst(tokens: Vec<FatToken>) -> Result<Cst, Error> {
    terminated(
        parse_cst,
        must(try_take_kind(FatTokenKind::EndOfFile, ErrorKind::Syntax)),
    )(&tokens)
    .finish()
    .map(|(_, cst)| cst)
}

fn parse_cst(tokens: &[FatToken]) -> IResult<Cst> {
    map(many0(parse_object), |objects| Cst { objects })(tokens)
}

fn parse_object(tokens: &[FatToken]) -> IResult<Object> {
    alt((
        map(inductive::parse_inductive, Object::Inductive),
        map(function::parse_function, Object::Function),
    ))(tokens)
}

#[cfg(test)]
pub mod test {
    use crate::cst::{Call, Expression, Function, Ident, InductiveDefinition};
    use crate::str_to_cst;

    use super::*;

    #[test]
    fn test_empty_cst_from_no_tokens() {
        assert_eq!(
            str_to_cst("").unwrap(),
            Cst {
                objects: Vec::new()
            }
        )
    }

    #[test]
    fn test_cst_of_multiple_ind_definitions() {
        assert_eq!(
            str_to_cst("ind nat {}\nind bool {}").unwrap(),
            Cst {
                objects: vec![
                    Object::Inductive(InductiveDefinition {
                        name: Ident::new(4, "nat"),
                        arguments: Vec::new(),
                        constructors: Vec::new(),
                    }),
                    Object::Inductive(InductiveDefinition {
                        name: Ident::new(15, "bool"),
                        arguments: Vec::new(),
                        constructors: Vec::new()
                    })
                ]
            }
        )
    }

    #[test]
    fn test_cst_of_ind_missing_name() {
        assert_eq!(
            str_to_cst("ind {True, False}").unwrap_err(),
            Error {
                kind: ErrorKind::ExpectedInductiveName,
                start: Some(4..5)
            }
        )
    }
    #[test]
    fn test_cst_of_unparsed_text() {
        assert_eq!(
            str_to_cst("in Bool {True, False}").unwrap_err(),
            Error {
                kind: ErrorKind::Syntax,
                start: Some(0..2)
            }
        )
    }

    #[test]
    fn test_cst_of_trivial_fn_definition_without_arguments() {
        assert_eq!(
            str_to_cst("fn drop() -> Bool {False}").unwrap(),
            Cst {
                objects: vec![Object::Function(Function {
                    name: Ident::new(3, "drop"),
                    arguments: vec![],
                    return_class: Ident::new(13, "Bool"),
                    body: Expression::Call(Call {
                        name: Ident::new(19, "False"),
                        arguments: None
                    })
                })]
            }
        )
    }
}
