use nom::{combinator::map, sequence::tuple};

use super::{
    expression::parse_expression,
    ident::parse_ident,
    parse::{chain_error, must, try_take_kind, IResult},
    ErrorKind,
};
use crate::token::{FatToken, Keyword};
use crate::{cst::Function, token::Symbol};

pub fn parse_function(tokens: &[FatToken]) -> IResult<Function> {
    map(
        tuple((
            try_take_kind(Keyword::Fn, ErrorKind::ExpectedKeyword(Keyword::Fn)),
            must(chain_error(parse_ident, ErrorKind::ExpectedFunctionName)),
            must(super::argument::parse_paren_argument_list),
            must(try_take_kind(
                Symbol::Arrow,
                ErrorKind::ExpectedSymbol(Symbol::Arrow),
            )),
            must(chain_error(
                parse_ident,
                ErrorKind::ExpectedFunctionReturnClass,
            )),
            must(try_take_kind(
                Symbol::OpenBrace,
                ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
            )),
            must(chain_error(
                parse_expression,
                ErrorKind::ExpectedFunctionBody,
            )),
            must(try_take_kind(
                Symbol::CloseBrace,
                ErrorKind::ExpectedSymbol(Symbol::CloseBrace),
            )),
        )),
        |(_, name, arguments, _, return_class, _, body, _)| Function {
            name,
            arguments,
            return_class,
            body,
        },
    )(tokens)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::cst::{Argument, Call, Expression, Ident};
    use crate::parser::error::Error;
    use nom::Finish;

    fn str_to_function(source: &str) -> Result<Function, Error> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        parse_function(&tokens).finish().map(|(_, tokens)| tokens)
    }

    #[test]
    fn empty_function_definition_gives_error() {
        assert_eq!(
            str_to_function(""),
            Err(Error {
                kind: ErrorKind::ExpectedKeyword(Keyword::Fn),
                start: Some(0..0)
            })
        )
    }

    #[test]
    fn test_expect_name_after_fn_keyword_error() {
        assert_eq!(
            str_to_function("fn"),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionName,
                start: Some(2..2)
            })
        );
        assert_eq!(
            str_to_function("fn {"),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionName,
                start: Some(3..4)
            })
        );
    }

    #[test]
    fn test_expect_open_parenthesis_after_fn_name_error() {
        assert_eq!(
            str_to_function("fn not"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenParen),
                start: Some(6..6)
            })
        );
        assert_eq!(
            str_to_function("fn not{"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenParen),
                start: Some(6..7)
            })
        );
    }

    #[test]
    fn test_expect_close_parenthesis_after_fn_parameters_error() {
        assert_eq!(
            str_to_function("fn not("),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseParen),
                start: Some(7..7)
            })
        );
        assert_eq!(
            str_to_function("fn not(}"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseParen),
                start: Some(7..8)
            })
        );
    }

    #[test]
    fn test_expect_arrow_after_fn_signature_error() {
        assert_eq!(
            str_to_function("fn not()"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::Arrow),
                start: Some(8..8)
            })
        );
        assert_eq!(
            str_to_function("fn not()*"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::Arrow),
                start: Some(8..9)
            })
        );
    }

    #[test]
    fn test_expect_return_class_after_arrow_error() {
        assert_eq!(
            str_to_function("fn not() -> "),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionReturnClass,
                start: Some(12..12)
            })
        );
        assert_eq!(
            str_to_function("fn not() -> {"),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionReturnClass,
                start: Some(12..13)
            })
        );
    }

    #[test]
    fn test_expect_open_brace_after_fn_signature_error() {
        assert_eq!(
            str_to_function("fn not() -> Void"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(16..16)
            })
        );
        assert_eq!(
            str_to_function("fn not() -> Void*"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(16..17)
            })
        );
    }

    #[test]
    fn test_expect_body_for_function_definition() {
        assert_eq!(
            str_to_function("fn not() -> Void {"),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionBody,
                start: Some(18..18)
            })
        );
        assert_eq!(
            str_to_function("fn not() -> Void {*"),
            Err(Error {
                kind: ErrorKind::ExpectedFunctionBody,
                start: Some(18..19)
            })
        );
    }

    #[test]
    fn test_expect_close_brace_for_function_definition() {
        assert_eq!(
            str_to_function("fn not() -> Bool {False"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseBrace),
                start: Some(23..23)
            })
        );
        assert_eq!(
            str_to_function("fn not() -> Bool {False*"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseBrace),
                start: Some(23..24)
            })
        );
    }

    #[test]
    fn test_cst_of_trivial_fn_definition() {
        assert_eq!(
            str_to_function("fn false() -> Bool {False}").unwrap(),
            Function {
                name: Ident::new(3, "false"),
                arguments: vec![],
                return_class: Ident::new(14, "Bool"),
                body: Expression::Call(Call {
                    name: Ident::new(20, "False"),
                    arguments: None
                })
            }
        )
    }

    #[test]
    fn test_cst_of_trivial_fn_definition_with_arguments() {
        assert_eq!(
            str_to_function("fn drop(a: Bool, b: Bool) -> Bool {False}").unwrap(),
            Function {
                name: Ident::new(3, "drop"),
                arguments: vec![
                    Argument {
                        name: Ident::new(8, "a"),
                        class: Ident::new(11, "Bool"),
                    },
                    Argument {
                        name: Ident::new(17, "b"),
                        class: Ident::new(20, "Bool"),
                    }
                ],
                return_class: Ident::new(29, "Bool"),
                body: Expression::Call(Call {
                    name: Ident::new(35, "False"),
                    arguments: None
                })
            }
        )
    }
}
