use nom::{combinator::map, sequence::tuple};

use super::{
    ident::parse_ident,
    parse::{must, parse_paren_comma_list, try_take_kind, IResult},
    ErrorKind,
};
use crate::token::FatToken;
use crate::{cst::Argument, token::Symbol};

pub fn parse_paren_argument_list(tokens: &[FatToken]) -> IResult<Vec<Argument>> {
    parse_paren_comma_list(parse_argument)(tokens)
}

fn parse_argument(tokens: &[FatToken]) -> IResult<Argument> {
    map(
        tuple((
            parse_ident,
            must(try_take_kind(
                Symbol::Colon,
                ErrorKind::ExpectedSymbol(Symbol::Colon),
            )),
            parse_ident,
        )),
        |(name, _, class)| Argument { name, class },
    )(tokens)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::cst::Argument;
    use crate::parser::error::Error;
    use nom::Finish;

    fn str_to_argument_list(source: &str) -> Result<Vec<Argument>, Error> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        parse_paren_argument_list(&tokens)
            .finish()
            .map(|(_, tokens)| tokens)
    }

    #[test]
    fn incorrect_arguments_list_errors() {
        assert_eq!(
            str_to_argument_list("(a)"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::Colon),
                start: Some(2..3)
            })
        );
        assert_eq!(
            str_to_argument_list("(a: Bool,)"),
            Err(Error {
                kind: ErrorKind::ExpectedIdent,
                start: Some(9..10)
            })
        );
    }
}
