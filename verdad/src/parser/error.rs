use crate::error::{Diagnostic, Label};
use crate::token::{FatToken, Keyword, Span};
use crate::Symbol;

#[derive(Debug, Eq, PartialEq)]
pub struct Error {
    pub kind: ErrorKind,
    pub start: Option<Span>,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum ErrorKind {
    ExpectedKeyword(Keyword),
    ExpectedSymbol(Symbol),
    ExpectedIdent,
    ExpectedInductiveName,
    ExpectedConstructor,
    ExpectedFunctionName,
    ExpectedFunctionBody,
    ExpectedFunctionReturnClass,
    ExpectedMatchArgument,
    ExpectedArmExpression,
    ExpectedCommaOrCloseBrace,
    Syntax,
}

impl std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ExpectedKeyword(keyword) => write!(f, "expected keyword `{}`", keyword.as_str()),
            Self::ExpectedSymbol(symbol) => write!(
                f,
                "expected {} symbol `{}`",
                symbol.english_name(),
                symbol.as_str()
            ),
            Self::ExpectedIdent => write!(f, "expected ident"),
            Self::ExpectedInductiveName => write!(f, "expected inductive name after `ind` keyword"),
            Self::ExpectedConstructor => write!(f, "expected constructor definintion"),
            Self::ExpectedFunctionName => write!(f, "expected function name after `fn` keyword"),
            Self::ExpectedFunctionBody => write!(f, "expected function body after `{{`"),
            Self::ExpectedFunctionReturnClass => {
                write!(f, "expected function return class after arrow")
            }
            Self::ExpectedMatchArgument => {
                write!(f, "expected argument name after `match` keyword")
            }
            Self::ExpectedArmExpression => {
                write!(f, "expected expression following arrow `->` in match arm")
            }
            Self::ExpectedCommaOrCloseBrace => {
                write!(f, "expected a comma `,` or a close brace `{{`")
            }
            Self::Syntax => write!(f, "syntax error"),
        }
    }
}

impl nom::error::ParseError<&[FatToken]> for Error {
    fn from_error_kind(tokens: &[FatToken], _: nom::error::ErrorKind) -> Self {
        Self {
            kind: ErrorKind::Syntax,
            start: tokens.first().map(|token| token.span.clone()),
        }
    }

    fn append(_: &[FatToken], _: nom::error::ErrorKind, other: Self) -> Self {
        other
    }
}

impl crate::Error for Error {
    fn diagnostic(&self, file_id: usize) -> Diagnostic {
        let range = self.start.clone().unwrap_or(0..0);
        Diagnostic::error()
            .with_message(self.kind.to_string())
            .with_labels(vec![
                Label::primary(file_id, range).with_message(format!("here: {}", self.kind))
            ])
    }

    fn span(&self) -> Span {
        self.start
            .clone()
            .unwrap_or(std::usize::MAX..std::usize::MAX)
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}
