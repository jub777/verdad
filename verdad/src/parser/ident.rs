use nom::combinator::map;

use crate::cst::Ident;
use crate::token::{FatToken, FatTokenKind};

use super::{error::ErrorKind, parse::try_take_if, parse::IResult};

pub fn parse_ident(tokens: &[FatToken]) -> IResult<Ident> {
    map(
        try_take_if(FatTokenKind::is_name, ErrorKind::ExpectedIdent),
        |name| Ident {
            span: name.span.clone(),
            symbol: name.kind.symbol().unwrap(),
        },
    )(tokens)
}
