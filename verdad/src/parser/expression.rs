use nom::{
    branch::alt,
    combinator::{map, opt},
    sequence::tuple,
};

use crate::{
    cst::{Arm, Call, Expression, Match, Pattern},
    token::{FatToken, Keyword, Symbol},
};

use super::{
    error::ErrorKind,
    ident::parse_ident,
    parse::{
        chain_error, must, parse_comma_list, parse_paren_comma_list, try_take_kind, IResult,
        TrailingComma,
    },
};

pub fn parse_expression(tokens: &[FatToken]) -> IResult<Expression> {
    alt((
        map(parse_call, Expression::Call),
        map(parse_match, Expression::Match),
    ))(tokens)
}

pub fn parse_call(tokens: &[FatToken]) -> IResult<Call> {
    map(
        tuple((parse_ident, opt(parse_paren_comma_list(parse_expression)))),
        |(name, arguments)| Call { name, arguments },
    )(tokens)
}

pub fn parse_match(tokens: &[FatToken]) -> IResult<Match> {
    map(
        tuple((
            try_take_kind(Keyword::Match, ErrorKind::ExpectedKeyword(Keyword::Match)),
            must(chain_error(parse_ident, ErrorKind::ExpectedMatchArgument)),
            must(try_take_kind(
                Symbol::OpenBrace,
                ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
            )),
            parse_arms_list,
            must(try_take_kind(
                Symbol::CloseBrace,
                ErrorKind::ExpectedCommaOrCloseBrace,
            )),
        )),
        |(start, argument, _, arms, end)| Match {
            argument,
            arms,
            span: start.span.start..end.span.end,
        },
    )(tokens)
}

fn parse_arms_list(tokens: &[FatToken]) -> IResult<Vec<Arm>> {
    parse_comma_list(parse_arm, TrailingComma::Optional)(tokens)
}

fn parse_arm(tokens: &[FatToken]) -> IResult<Arm> {
    map(
        tuple((
            parse_pattern,
            must(try_take_kind(
                Symbol::Arrow,
                ErrorKind::ExpectedSymbol(Symbol::Arrow),
            )),
            must(chain_error(
                parse_expression,
                ErrorKind::ExpectedArmExpression,
            )),
        )),
        |(pattern, _, expression)| Arm {
            pattern,
            expression,
        },
    )(tokens)
}

fn parse_pattern(tokens: &[FatToken]) -> IResult<Pattern> {
    map(
        tuple((parse_ident, opt(parse_paren_comma_list(parse_ident)))),
        |(name, bindings)| Pattern {
            name,
            bindings: bindings.unwrap_or_default(),
        },
    )(tokens)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::cst::{Call, Ident, Pattern};
    use crate::parser::error::Error;
    use nom::Finish;

    fn str_to_expression(source: &str) -> Result<Expression, Error> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        parse_expression(&tokens).finish().map(|(_, tokens)| tokens)
    }

    #[test]
    fn cst_of_trivial_call_expression() {
        assert_eq!(
            str_to_expression("False").unwrap(),
            Expression::Call(Call {
                name: Ident::new(0, "False"),
                arguments: None
            })
        )
    }

    #[test]
    fn cst_of_nested_call_expression() {
        assert_eq!(
            str_to_expression("Inc(Zero)").unwrap(),
            Expression::Call(Call {
                name: Ident::new(0, "Inc"),
                arguments: Some(vec![Expression::Call(Call {
                    name: Ident::new(4, "Zero"),
                    arguments: None
                })])
            })
        )
    }

    #[test]
    fn error_if_missing_match_argument() {
        assert_eq!(
            str_to_expression("match "),
            Err(Error {
                kind: ErrorKind::ExpectedMatchArgument,
                start: Some(6..6)
            })
        );
        assert_eq!(
            str_to_expression("match {"),
            Err(Error {
                kind: ErrorKind::ExpectedMatchArgument,
                start: Some(6..7)
            })
        );
    }

    #[test]
    fn error_if_missing_match_open_brace() {
        assert_eq!(
            str_to_expression("match a"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(7..7)
            })
        );
        assert_eq!(
            str_to_expression("match a then"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(8..12)
            })
        );
    }

    #[test]
    fn error_if_missing_match_close_brace() {
        assert_eq!(
            str_to_expression("match a {"),
            Err(Error {
                kind: ErrorKind::ExpectedCommaOrCloseBrace,
                start: Some(9..9)
            })
        );
        assert_eq!(
            str_to_expression("match a {{"),
            Err(Error {
                kind: ErrorKind::ExpectedCommaOrCloseBrace,
                start: Some(9..10)
            })
        );
    }

    #[test]
    fn test_cst_of_trivial_match_definition() {
        assert_eq!(
            str_to_expression("match a{}").unwrap(),
            Expression::Match(Match {
                argument: Ident::new(6, "a"),
                arms: Vec::new(),
                span: (0..9)
            })
        )
    }

    #[test]
    fn error_if_pattern_not_followed_by_arrow() {
        assert_eq!(
            str_to_expression("match a {Arg}"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::Arrow),
                start: Some(12..13)
            })
        );
        assert_eq!(
            str_to_expression("match a {Arg"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::Arrow),
                start: Some(12..12)
            })
        );
    }

    #[test]
    fn error_if_arrow_not_followed_by_expression() {
        assert_eq!(
            str_to_expression("match a {Arg->}"),
            Err(Error {
                kind: ErrorKind::ExpectedArmExpression,
                start: Some(14..15)
            })
        );
        assert_eq!(
            str_to_expression("match a {Arg->"),
            Err(Error {
                kind: ErrorKind::ExpectedArmExpression,
                start: Some(14..14)
            })
        );
    }

    #[test]
    fn test_cst_of_single_arm_match_definition() {
        assert_eq!(
            str_to_expression("match a{True->False}").unwrap(),
            Expression::Match(Match {
                argument: Ident::new(6, "a"),
                arms: vec![Arm {
                    pattern: Pattern {
                        name: Ident::new(8, "True"),
                        bindings: vec![]
                    },
                    expression: Expression::Call(Call {
                        name: Ident::new(14, "False"),
                        arguments: None
                    })
                }],
                span: (0..20)
            })
        )
    }

    #[test]
    fn error_if_pattern_open_paren_not_followed_by_close_or_binding() {
        assert_eq!(
            str_to_expression("match a {False(->"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseParen),
                start: Some(15..17)
            })
        );
        assert_eq!(
            str_to_expression("match a {False("),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseParen),
                start: Some(15..15)
            })
        );
    }

    #[test]
    fn pattern_bindings_in_match_arm_ok() {
        assert_eq!(
            str_to_expression("match a {Zero()->Zero,Inc(n)->Zero}").unwrap(),
            Expression::Match(Match {
                argument: Ident::new(6, "a"),
                arms: vec![
                    Arm {
                        pattern: Pattern {
                            name: Ident::new(9, "Zero"),
                            bindings: vec![]
                        },
                        expression: Expression::Call(Call {
                            name: Ident::new(17, "Zero"),
                            arguments: None
                        }),
                    },
                    Arm {
                        pattern: Pattern {
                            name: Ident::new(22, "Inc"),
                            bindings: vec![Ident::new(26, "n")]
                        },
                        expression: Expression::Call(Call {
                            name: Ident::new(30, "Zero"),
                            arguments: None
                        }),
                    }
                ],
                span: (0..35)
            })
        );
    }

    #[test]
    fn test_cst_of_match_arm_containing_match() {
        assert_eq!(
            str_to_expression("match a{True->match a{}, False->match a{}}").unwrap(),
            Expression::Match(Match {
                argument: Ident::new(6, "a"),
                arms: vec![
                    Arm {
                        pattern: Pattern {
                            name: Ident::new(8, "True"),
                            bindings: vec![]
                        },
                        expression: Expression::Match(Match {
                            argument: Ident::new(20, "a"),
                            arms: Vec::new(),
                            span: (14..23)
                        }),
                    },
                    Arm {
                        pattern: Pattern {
                            name: Ident::new(25, "False"),
                            bindings: vec![]
                        },
                        expression: Expression::Match(Match {
                            argument: Ident::new(38, "a"),
                            arms: Vec::new(),
                            span: (32..41)
                        }),
                    }
                ],
                span: (0..42)
            })
        )
    }

    #[test]
    fn test_cst_of_match_arms_with_missing_comma() {
        assert_eq!(
            str_to_expression("match a{True->match a{} False->match a{}}"),
            Err(Error {
                kind: ErrorKind::ExpectedCommaOrCloseBrace,
                start: Some(24..29)
            })
        )
    }
}
