use nom::{
    combinator::{map, opt},
    sequence::tuple,
};

use super::{
    constructor::parse_constructors,
    ident::parse_ident,
    parse::{chain_error, must, try_take_kind, IResult},
    ErrorKind,
};
use crate::cst::InductiveDefinition;
use crate::token::{FatToken, Keyword, Symbol};

pub fn parse_inductive(tokens: &[FatToken]) -> IResult<InductiveDefinition> {
    map(
        tuple((
            try_take_kind(Keyword::Ind, ErrorKind::ExpectedKeyword(Keyword::Ind)),
            must(chain_error(parse_ident, ErrorKind::ExpectedInductiveName)),
            opt(super::argument::parse_paren_argument_list),
            must(try_take_kind(
                Symbol::OpenBrace,
                ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
            )),
            parse_constructors,
            must(try_take_kind(
                Symbol::CloseBrace,
                ErrorKind::ExpectedSymbol(Symbol::CloseBrace),
            )),
        )),
        |(_, name, arguments, _, constructors, _)| InductiveDefinition {
            name,
            arguments: arguments.unwrap_or_default(),
            constructors,
        },
    )(tokens)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::cst::{Argument, Constructor, Ident};
    use crate::parser::error::Error;
    use nom::Finish;

    fn str_to_inductive(source: &str) -> Result<InductiveDefinition, Error> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        parse_inductive(&tokens).finish().map(|(_, tokens)| tokens)
    }

    #[test]
    fn test_expect_name_after_ind_keyword_error() {
        assert_eq!(
            str_to_inductive("ind"),
            Err(Error {
                kind: ErrorKind::ExpectedInductiveName,
                start: Some(3..3)
            })
        );
        assert_eq!(
            str_to_inductive("ind {"),
            Err(Error {
                kind: ErrorKind::ExpectedInductiveName,
                start: Some(4..5)
            })
        );
    }

    #[test]
    fn test_expect_open_bracket_after_ind_name_error() {
        assert_eq!(
            str_to_inductive("ind Nat"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(7..7)
            })
        );
    }

    #[test]
    fn test_unfinished_empty_ind_error() {
        assert_eq!(
            str_to_inductive("ind Nat {"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseBrace),
                start: Some(9..9)
            })
        );
    }

    #[test]
    fn test_trivial_empty_ind_definition_success() {
        assert_eq!(
            str_to_inductive("ind Nat {}"),
            Ok(InductiveDefinition {
                name: Ident::new(4, "Nat"),
                arguments: Vec::new(),
                constructors: Vec::new(),
            })
        );
    }

    #[test]
    fn test_expect_close_parenthasis_after_ind_name_error() {
        assert_eq!(
            str_to_inductive("ind Nat("),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::CloseParen),
                start: Some(8..8)
            })
        );
    }

    #[test]
    fn test_expect_open_brace_after_ind_arguments_error() {
        assert_eq!(
            str_to_inductive("ind Nat()"),
            Err(Error {
                kind: ErrorKind::ExpectedSymbol(Symbol::OpenBrace),
                start: Some(9..9)
            })
        );
    }

    #[test]
    fn test_ind_type_with_arguments() {
        assert_eq!(
            str_to_inductive("ind Equal(a: Bool, b: Bool) {}"),
            Ok(InductiveDefinition {
                name: Ident::new(4, "Equal"),
                arguments: vec![
                    Argument {
                        name: Ident::new(10, "a"),
                        class: Ident::new(13, "Bool")
                    },
                    Argument {
                        name: Ident::new(19, "b"),
                        class: Ident::new(22, "Bool")
                    }
                ],
                constructors: vec![]
            })
        );
    }

    #[test]
    fn test_single_trivial_constructor_ind_definition() {
        assert_eq!(
            str_to_inductive("ind Nat {O}"),
            Ok(InductiveDefinition {
                name: Ident::new(4, "Nat"),
                arguments: Vec::new(),
                constructors: vec![Constructor {
                    name: Ident::new(9, "O"),
                    arguments: Vec::new(),
                    class: None
                }]
            })
        );
    }

    #[test]
    fn test_single_trivial_constructor_with_trailing_comma_ind_definition() {
        assert_eq!(
            str_to_inductive("ind Nat {O,}"),
            Ok(InductiveDefinition {
                name: Ident::new(4, "Nat"),
                arguments: Vec::new(),
                constructors: vec![Constructor {
                    name: Ident::new(9, "O"),
                    arguments: Vec::new(),
                    class: None
                }]
            })
        );
    }
}
