use nom::combinator::opt;
use nom::sequence::{pair, preceded};
use nom::{combinator::map, sequence::tuple};

use crate::cst::Call;
use crate::token::FatToken;
use crate::{cst::Constructor, Symbol};

use super::parse::parse_paren_comma_list;
use super::{
    error::ErrorKind,
    ident::parse_ident,
    parse::{chain_error, must, parse_comma_list, try_take_kind, IResult, TrailingComma},
};

pub fn parse_constructors(tokens: &[FatToken]) -> IResult<Vec<Constructor>> {
    parse_comma_list(parse_constructor, TrailingComma::Optional)(tokens)
}

fn parse_constructor(tokens: &[FatToken]) -> IResult<Constructor> {
    map(
        tuple((
            chain_error(parse_ident, ErrorKind::ExpectedConstructor),
            opt(super::argument::parse_paren_argument_list),
            opt(preceded(
                try_take_kind(Symbol::Colon, ErrorKind::ExpectedSymbol(Symbol::Colon)),
                must(parse_inductive_call),
            )),
        )),
        |(name, arguments, class)| Constructor {
            name,
            arguments: arguments.unwrap_or_default(),
            class,
        },
    )(tokens)
}

fn parse_inductive_call(tokens: &[FatToken]) -> IResult<Call> {
    map(
        pair(
            parse_ident,
            opt(parse_paren_comma_list(super::expression::parse_expression)),
        ),
        |(name, arguments)| Call { name, arguments },
    )(tokens)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::cst::{Argument, Call, Expression, Ident};
    use crate::parser::error::Error;
    use nom::Finish;

    fn str_to_constructors(source: &str) -> Result<Vec<Constructor>, Error> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        parse_constructors(&tokens)
            .finish()
            .map(|(_, tokens)| tokens)
    }

    #[test]
    fn no_constructors() {
        assert_eq!(str_to_constructors(""), Ok(vec![]))
    }

    #[test]
    fn single_trivial_ind_constructor() {
        assert_eq!(
            str_to_constructors("O"),
            Ok(vec![Constructor {
                name: Ident::new(0, "O"),
                arguments: Vec::new(),
                class: None
            }])
        )
    }

    #[test]
    fn two_trivial_ind_constructors() {
        assert_eq!(
            str_to_constructors("True, False"),
            Ok(vec![
                Constructor {
                    name: Ident::new(0, "True"),
                    arguments: Vec::new(),
                    class: None
                },
                Constructor {
                    name: Ident::new(6, "False"),
                    arguments: Vec::new(),
                    class: None
                }
            ])
        )
    }

    #[test]
    fn partially_typed_constructor_errors() {
        assert_eq!(
            str_to_constructors("True:, False"),
            Err(Error {
                kind: ErrorKind::ExpectedIdent,
                start: Some(5..6)
            })
        )
    }

    #[test]
    fn constructor_with_explicit_type() {
        assert_eq!(
            str_to_constructors("True: Bool"),
            Ok(vec![Constructor {
                name: Ident::new(0, "True"),
                arguments: Vec::new(),
                class: Some(Call {
                    name: Ident::new(6, "Bool"),
                    arguments: None
                })
            }])
        )
    }

    #[test]
    fn constructor_with_explicit_type_with_arguments() {
        assert_eq!(
            str_to_constructors("Reflex(a: Int): Eq(a)"),
            Ok(vec![Constructor {
                name: Ident::new(0, "Reflex"),
                arguments: vec![Argument {
                    name: Ident::new(7, "a"),
                    class: Ident::new(10, "Int")
                }],
                class: Some(Call {
                    name: Ident::new(16, "Eq"),
                    arguments: Some(vec![Expression::Call(Call {
                        name: Ident::new(19, "a"),
                        arguments: None
                    })])
                })
            }])
        )
    }

    #[test]
    fn constructor_with_arguments() {
        assert_eq!(
            str_to_constructors("Zero(), Nplus(n: Int)"),
            Ok(vec![
                Constructor {
                    name: Ident::new(0, "Zero"),
                    arguments: Vec::new(),
                    class: None
                },
                Constructor {
                    name: Ident::new(8, "Nplus"),
                    arguments: vec![Argument {
                        name: Ident::new(14, "n"),
                        class: Ident::new(17, "Int")
                    }],
                    class: None
                }
            ])
        )
    }
}
