use super::error::{Error, ErrorKind};
use crate::token::{FatToken, FatTokenKind, Symbol};
pub use nom::combinator::cut as must;
use nom::{sequence::delimited, Err};

pub type IResult<'a, T> = nom::IResult<&'a [FatToken], T, Error>;

pub fn chain_error<'t, 'p, O, P>(
    mut parser: P,
    kind: ErrorKind,
) -> impl FnMut(&'t [FatToken]) -> IResult<'t, O> + 'p
where
    P: FnMut(&'t [FatToken]) -> IResult<O> + Copy + 'p,
{
    move |tokens: &'t [FatToken]| {
        let mut result = parser(tokens);
        if let IResult::Err(Err::Error(error)) = &mut result {
            error.kind = kind;
        }
        result
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum TrailingComma {
    Optional,
    Forbidden,
}

pub fn parse_paren_comma_list<'p, 't: 'p, O, P>(
    item_parser: P,
) -> impl FnMut(&'t [FatToken]) -> IResult<'t, Vec<O>> + 'p
where
    P: FnMut(&'t [FatToken]) -> IResult<'t, O> + Copy + 'p,
    O: 'static,
{
    delimited(
        try_take_kind(
            Symbol::OpenParen,
            ErrorKind::ExpectedSymbol(Symbol::OpenParen),
        ),
        must(parse_comma_list(item_parser, TrailingComma::Forbidden)),
        must(try_take_kind(
            Symbol::CloseParen,
            ErrorKind::ExpectedSymbol(Symbol::CloseParen),
        )),
    )
}

pub fn parse_comma_list<'t, 'p, O, P>(
    mut item_parser: P,
    trailing_comma: TrailingComma,
) -> impl FnMut(&'t [FatToken]) -> IResult<'t, Vec<O>> + 'p
where
    P: FnMut(&'t [FatToken]) -> IResult<O> + Copy + 'p,
{
    move |tokens: &'t [FatToken]| {
        let mut outputs = Vec::new();

        let (mut tokens, first) = match item_parser(tokens) {
            IResult::Err(Err::Error(_)) => return IResult::Ok((tokens, outputs)),
            IResult::Err(error) => return IResult::Err(error),
            IResult::Ok((tokens, first)) => (tokens, first),
        };

        outputs.push(first);

        while let Some(next_tokens) = skip_if_comma(tokens) {
            let (next_tokens, next) = match item_parser(next_tokens) {
                IResult::Err(Err::Error(_)) if TrailingComma::Optional == trailing_comma => {
                    return IResult::Ok((next_tokens, outputs))
                }
                IResult::Err(Err::Error(error)) => return IResult::Err(Err::Failure(error)),
                IResult::Err(error) => return IResult::Err(error),
                IResult::Ok((next_tokens, first)) => (next_tokens, first),
            };
            outputs.push(next);
            tokens = next_tokens;
        }

        IResult::Ok((tokens, outputs))
    }
}

fn skip_if_comma(tokens: &[FatToken]) -> Option<&[FatToken]> {
    next_matches(
        tokens,
        |kind| kind == &FatTokenKind::Symbol(Symbol::Comma),
        ErrorKind::Syntax,
    )
    .ok()
    .map(|(tokens, _comma)| tokens)
}

pub fn try_take_kind<T: Into<FatTokenKind>>(
    into_kind: T,
    error_kind: ErrorKind,
) -> impl Fn(&[FatToken]) -> IResult<&FatToken> {
    let kind = into_kind.into();
    try_take_if(move |token_kind| token_kind == &kind, error_kind)
}

pub fn try_take_if<P>(
    is_matching_token: P,
    error_kind: ErrorKind,
) -> impl Fn(&[FatToken]) -> IResult<&FatToken>
where
    P: Copy + Fn(&FatTokenKind) -> bool,
{
    move |tokens: &[FatToken]| {
        next_matches(tokens, is_matching_token, error_kind).map_err(nom::Err::Error)
    }
}

fn next_matches<P>(
    input: &[FatToken],
    is_matching_token: P,
    error_kind: ErrorKind,
) -> Result<(&[FatToken], &FatToken), Error>
where
    P: Fn(&FatTokenKind) -> bool,
{
    match input.get(0) {
        Some(token) if is_matching_token(&token.kind) => Ok((&input[1..], token)),
        Some(token) => Err(Error {
            kind: error_kind,
            start: Some(token.span.clone()),
        }),
        None => Err(Error {
            kind: error_kind,
            start: None,
        }),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use nom::{combinator::map, Err};

    #[test]
    fn test_parse_optional_comma_list_no_trailing() {
        assert_eq!(str_to_list("", TrailingComma::Forbidden), Ok(Vec::new()));
        assert_eq!(str_to_list("A", TrailingComma::Forbidden), Ok(vec![0]));
        assert_eq!(
            str_to_list("A,", TrailingComma::Forbidden),
            Err(Err::Failure(Error {
                start: Some(2..2),
                kind: ErrorKind::ExpectedConstructor,
            }))
        );
        assert_eq!(str_to_list("A,B", TrailingComma::Forbidden), Ok(vec![0, 2]));
        assert_eq!(
            str_to_list("A,}", TrailingComma::Forbidden),
            Err(Err::Failure(Error {
                start: Some(2..3),
                kind: ErrorKind::ExpectedConstructor,
            }))
        );
    }

    #[test]
    fn test_parse_optional_comma_list_with_trailing() {
        assert_eq!(str_to_list("", TrailingComma::Optional), Ok(Vec::new()));
        assert_eq!(str_to_list("A", TrailingComma::Optional), Ok(vec![0]));
        assert_eq!(str_to_list("A,", TrailingComma::Optional), Ok(vec![0]));
        assert_eq!(str_to_list("A,}", TrailingComma::Optional), Ok(vec![0]));
        assert_eq!(str_to_list("A,B", TrailingComma::Optional), Ok(vec![0, 2]));
    }

    fn str_to_list(source: &str, trailing_comma: TrailingComma) -> Result<Vec<usize>, Err<Error>> {
        let tokens = crate::interner::test::str_to_fat_tokens(source);
        let result = parse_comma_list(parse_foo, trailing_comma)(&tokens).map(|(_, tokens)| tokens);
        result
    }

    fn parse_foo(tokens: &[FatToken]) -> IResult<usize> {
        map(
            try_take_if(FatTokenKind::is_name, ErrorKind::ExpectedConstructor),
            |token| token.span.start,
        )(tokens)
    }
}
